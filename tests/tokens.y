%{
#include <string>
#include <iostream>
#include <stdio.h>

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int yylineno;

void yyerror(const char *s);
extern "C" int yywrap() {
    return 1;
}
void Print(std::string input) {
    std::cout << input << std::endl;
}
%}

%union {
    int ival;
    char* sval;
}

%token TOK_PLUS
%token TOK_MINUS
%token TOK_TIMES
%token TOK_DIVIDE
%token TOK_BRACKET_L
%token TOK_BRACKET_R
%token TOK_LESSEQUAL
%token TOK_LESS
%token TOK_GREATEQUAL
%token TOK_GREAT
%token TOK_COLON
%token TOK_SEMICOLON
%token TOK_COMMA
%token TOK_DOT
%token TOK_PARENT_BRACKET_L
%token TOK_PARENT_BRACKET_R
%token TOK_AND
%token TOK_OR
%token TOK_EQUAL
%token TOK_NOT_EQUAL
%token TOK_GREAT_EQUAL
%token TOK_LESS_EQUAL
%token TOK_NOT
%token TOK_ASSIGN

%token TOK_KW_VOID
%token TOK_KW_CLASS
%token TOK_KW_ELSE
%token TOK_KW_IF
%token TOK_KW_INT
%token TOK_KW_NEW
%token TOK_KW_RETURN
%token TOK_KW_STRING
%token TOK_KW_SUPER
%token TOK_KW_THIS
%token TOK_KW_WHILE

%token TOK_INVALID

%token <ival> TOK_INT
%token <sval> TOK_STRING
%token <sval> TOK_ID

%start program
%%

program:
    TOK_PLUS { Print("PLUS"); } program
    | TOK_MINUS { Print("MINUS"); } program
    | TOK_TIMES { Print("TIMES"); } program
    | TOK_DIVIDE { Print("DIVIDE"); } program
    | TOK_PARENT_BRACKET_L { Print("PARENT_BRACKET_L"); } program
    | TOK_PARENT_BRACKET_R { Print("PARENT_BRACKET_R"); } program
    | TOK_LESS_EQUAL { Print("LESS_EQUAL"); } program
    | TOK_LESS { Print("LESS"); } program
    | TOK_GREAT_EQUAL { Print("GREAT_EQUAL"); } program
    | TOK_GREAT { Print("GREAT"); } program
    | TOK_COLON { Print("COLON"); } program
    | TOK_SEMICOLON { Print("SEMICOLON"); } program
    | TOK_COMMA { Print("COMMA"); } program
    | TOK_DOT { Print("DOT"); } program
    | TOK_BRACKET_L { Print("BRACKET_L"); } program
    | TOK_BRACKET_R { Print("BRACKET_R"); } program
    | TOK_AND { Print("AND"); } program
    | TOK_OR { Print("OR"); } program
    | TOK_EQUAL { Print("EQUAL"); } program
    | TOK_NOT_EQUAL { Print("NOTEQUAL"); } program
    | TOK_NOT { Print("NOT"); } program
    | TOK_ASSIGN { Print("ASSIGN"); } program
    | TOK_KW_VOID { Print("KW_VOID"); } program
    | TOK_KW_CLASS { Print("KW_CLASS"); } program
    | TOK_KW_ELSE { Print("KW_ELSE"); } program
    | TOK_KW_IF { Print("KW_IF"); } program
    | TOK_KW_INT { Print("KW_INT"); } program
    | TOK_KW_NEW { Print("KW_NEW"); } program
    | TOK_KW_RETURN { Print("KW_RETURN"); } program
    | TOK_KW_STRING { Print("KW_STRING"); } program
    | TOK_KW_SUPER { Print("KW_SUPER"); } program
    | TOK_KW_THIS { Print("KW_THIS"); } program
    | TOK_KW_WHILE { Print("KW_WHILE"); } program
    | TOK_INT { std::cout << "INT " << $1 << std::endl; } program
    | TOK_STRING { std::cout << "STRING " << $1 << std::endl; } program
    | TOK_ID { std::cout << "ID " << $1 << std::endl; } program
    | TOK_INVALID {yyerror("Invalid token detected - exiting");}
    | { Print("THE_END"); }
    ;

%%

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Compiler requiers exactly one argument" << std::endl;
        return -1;
    }
    FILE *f = fopen(argv[1], "r");
    if (!f)
    {
        std::cerr << "Error while opening input file" << std::endl;
        return -2;
    }
    yyin = f;
    while (!feof(yyin))
    {
        yyparse();
    }

}

void yyerror(const char *s) {
    std::cout << "Parser error (line: " << yylineno << "): " << s << std::endl;
    exit(-1);
}
void the_end() {}

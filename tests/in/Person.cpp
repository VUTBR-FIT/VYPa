class Person: Object {
    string name,
        surname;

    int age;

    Person mother,
        father;

    void Person(void) {
        this.name = '';
        this.surname = '';
    }

    Person setAge(int age) {
        this.age = age;
    }

    Person setName(string name) {
        this.name = name;
    }

    Person setSurname(string surname) {
        this.surname = surname;
    }

    int getAge(void) {
        return this.age;
    }

    string getName(void) {
        return this.name;
    }

    string getSurname(void) {
        return this.surname;
    }

    string isChild(void) {
        if (this.age < 18) {
            return "NE";
        } else {
            return "ANO"
        }
    }

};
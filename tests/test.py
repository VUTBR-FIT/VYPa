#!/usr/bin/env python

import sys
import subprocess
import os

# Configure command line output
NO_BG = '\033[0m'
ERROR = '\033[1;31m'
SUCCESS = '\033[1;32m'

# Configure program
inputDir = os.path.join(os.path.abspath("."), "tests", "in")
refOutDir = os.path.join(os.path.abspath("."), "tests", "ref-out")
outputDir = os.path.join(os.path.abspath("."), "tests", "out")

program = os.path.join(os.path.abspath("."), "comp")

# One test configure
# 1 => input
# 2 => ref-out (default: same name as input)
# 3 => output (default: same mane as input)
# 4 => return code (default:  ERRORS.get("success)

ERRORS = {
    "success": 0,
    "e_lexical": 1,
    "e_syntax": 2,
    "e_semantic": 3,
    "e_sem_check": 4,
    "e_generator": 5,
    "e_internal": 9
}

tokens = [
    ['tok_and.txt',None,None, ERRORS.get("success")],
    ['tok_assign.txt',None,None, ERRORS.get("success")],
    ['tok_bracket_l.txt',None,None, ERRORS.get("success")],
    ['tok_bracket_r.txt',None,None, ERRORS.get("success")],
    ['tok_colon.txt',None,None, ERRORS.get("success")],
    ['tok_comma.txt',None,None, ERRORS.get("success")],
    ['tok_divide.txt',None,None, ERRORS.get("success")],
    ['tok_dot.txt',None,None, ERRORS.get("success")],
    ['tok_equal.txt',None,None, ERRORS.get("success")],
    ['tok_great.txt',None,None, ERRORS.get("success")],
    ['tok_great_equal.txt',None,None, ERRORS.get("success")],
    ['tok_id.txt',None,None, ERRORS.get("success")],
    ['tok_id_0.txt',None,None, ERRORS.get("success")],
    ['tok_id_1.txt',None,None, ERRORS.get("success")],
    ['tok_id_2.txt',None,None, ERRORS.get("success")],
    ['tok_int.txt',None,None, ERRORS.get("success")],
    ['tok_kw_class.txt',None,None, ERRORS.get("success")],
    ['tok_kw_else.txt',None,None, ERRORS.get("success")],
    ['tok_kw_if.txt',None,None, ERRORS.get("success")],
    ['tok_kw_int.txt',None,None, ERRORS.get("success")],
    ['tok_kw_new.txt',None,None, ERRORS.get("success")],
    ['tok_kw_return.txt',None,None, ERRORS.get("success")],
    ['tok_kw_string.txt',None,None, ERRORS.get("success")],
    ['tok_kw_super.txt',None,None, ERRORS.get("success")],
    ['tok_kw_this.txt',None,None, ERRORS.get("success")],
    ['tok_kw_void.txt',None,None, ERRORS.get("success")],
    ['tok_kw_while.txt',None,None, ERRORS.get("success")],
    ['tok_less.txt',None,None, ERRORS.get("success")],
    ['tok_less_equal.txt',None,None, ERRORS.get("success")],
    ['tok_minus.txt',None,None, ERRORS.get("success")],
    ['tok_not.txt',None,None, ERRORS.get("success")],
    ['tok_notequal.txt',None,None, ERRORS.get("success")],
    ['tok_or.txt',None,None, ERRORS.get("success")],
    ['tok_parent_bracket_l.txt',None,None, ERRORS.get("success")],
    ['tok_parent_bracket_r.txt',None,None, ERRORS.get("success")],
    ['tok_plus.txt',None,None, ERRORS.get("success")],
    ['tok_semicolon.txt',None,None, ERRORS.get("success")],
    ['tok_string.txt',None,None, ERRORS.get("success")],
    ['tok_times.txt',None,None, ERRORS.get("success")],
    ['tok_kw_01.txt',None,None, ERRORS.get("success")],
]
syntax = [
    ['function.txt', 'empty.txt', None,  ERRORS.get("success")],
    ['function_arg.txt', 'empty.txt', None,  ERRORS.get("success")],
    ['function_args.txt', 'empty.txt', None,  ERRORS.get("success")],
    ['class.txt', 'empty.txt', None,  ERRORS.get("success")],
    ['class_err.txt', None, None, ERRORS.get("e_syntax")],
    ['class_method.txt', 'empty.txt', None,  ERRORS.get("success")],
    # FIXME ['Person.cpp', 'empty.txt', None,  ERRORS.get("success")],
]
semantic = [
    ['main.txt',None,None, ERRORS.get("success")],
    ['main_err.txt',None,None, ERRORS.get("e_semantic")],
    ['main.txt',None,None, ERRORS.get("e_semantic")],
]
count = 0
success = 0
error = 0

class ReturnCode(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class DiffFail(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


def runTest (inputFile, refOut=None, output=None, returnCode= ERRORS.get("success")):
    global count, error, success
    count += 1

    if refOut == None:
        refOut = inputFile
    if output == None:
        output = inputFile

    try:
        command = program + ' ' + os.path.join(inputDir, inputFile) + ' 2>&1 >' + os.path.join(outputDir, output)
        process = subprocess.Popen(command, shell=True)
        process.wait()

        if process.returncode != returnCode:
            raise ReturnCode()
        if returnCode ==  ERRORS.get("success"):
            diff = 'diff --strip-trailing-cr {} {} > {}'.format(os.path.join(refOutDir, refOut), os.path.join(outputDir, output), os.path.join(outputDir, (output + '.diff')))
            diffProcess = subprocess.Popen(diff, shell=True)
            diffProcess.wait()
            if diffProcess.returncode !=  ERRORS.get("success"):
                raise DiffFail()
            print SUCCESS + 'Test {:-4d} {:40} OK (DIFF)'.format(count, inputFile) + NO_BG
        else:
            print SUCCESS + 'Test {:-4d} {:40} OK (RETURN CODE)'.format(count, inputFile) + NO_BG
        success += 1
    except ReturnCode as e:
        error += 1
        print ERROR + 'Test {:-4d} {:40} FAILED (Return code: {:-3d}, expected: {:-3d}'.format(count, inputFile, process.returncode, returnCode) + NO_BG
    except DiffFail as e:
        error += 1
        print ERROR + 'Test {:-4d} {:40} FAILED (diff output)'.format(count, inputFile, process.returncode, returnCode) + NO_BG

process = subprocess.Popen("make test_tokens", shell=True)
process.wait()
for test in tokens:
    runTest(test[0],test[1],test[2],test[3])

process = subprocess.Popen("make test_syntax", shell=True)
process.wait()
for test in syntax:
    runTest(test[0],test[1],test[2],test[3])

print "STATS:"
print "\tSUCCESS: " + str(success)
print "\tERROR:   " + str(error)

if error !=  ERRORS.get("success"):
    sys.exit(1)

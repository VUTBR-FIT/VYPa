CC=g++
#FLAGS= -Wall -Wextra -pedantic -g -std=c++11 #DEBUG FLAGS
FLAGS= -std=c++11
BISON_FLAGS=
FLEX_FLAGS=
BISON_SOURCE = src/comp.y

all: clear build_dir dep comp

build_dir:
	@mkdir build/

build/comp.tab.c build/comp.tab.h: $(BISON_SOURCE)
	bison -d $(BISON_FLAGS) -o build/comp.tab.c $(BISON_SOURCE)

build/lex.yy.c: src/comp.l build/comp.tab.h
	flex $(FLEX_FLAGS) --yylineno -o $@ src/comp.l

comp: build/comp.tab.c build/lex.yy.c parser.o instruction_tape.o symbol_table.o expr_analyzer.o
	g++ $(FLAGS)  build/comp.tab.c build/lex.yy.c build/parser.o build/instruction_tape.o build/symbol_table.o build/expr_analyzer.o -o comp


clear:
	@rm -fr build
	@rm -f comp
	@rm -f comp.exe
	@rm -f dep.list
	@rm -f tests/out/*

clean: clear

# Tests
test:
	@rm -f tests/out/*
	python ./tests/test.py

test_tokens: BISON_SOURCE = tests/tokens.y
test_tokens: FLAGS += -D DEBUG_LEX
test_tokens: all

test_syntax: BISON_FLAGS=-v
test_syntax: FLAGS +=  -D DEBUG_SYNTAX
test_syntax: all

test_symbol_table: FLAGS +=  -D DEBUG_SYM_TABLE
test_symbol_table: all


# UNIVERSAL RULE
%.o : src/%.cpp
	$(CC) $(FLAGS) -c $< -o build/$@

# DEPENDENS GENERATOR
dep:
	$(CC) $(FLAGS) -MM src/*.cpp >dep.list
-include dep.list

FROM centos:7

RUN yum update -y \
    yum install -y build-essential ca-certificates curl git autoconf zip \
    && yum groupinstall -y "Development Tools" \
    && rm -fr /var/lib/apt/lists/*

ENV GCC_VERSION 7.3.0
ENV GPP_VERSION 7.3.0
ENV FLEX_VERSION 2.5.37
ENV BISON_VERSION 3.0.4

# RUN curl -o /root/gcc-$GCC_VERSION.tar.xz -fSL \
#     http://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz
# RUN curl -o /root/gcc-$GCC_VERSION.tar.xz.sig -fSL \
#     http://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz.sig
# RUN gpg --keyserver keys.gnupg.net --recv-keys 78D5264E \
#     && gpg --verify /root/gcc-$GCC_VERSION.tar.xz.sig \
# RUN cd /root \
#     && tar -xf gcc-$GCC_VERSION.tar.xz \
#     && cd gcc-$GCC_VERSION \
#     && ./configure --disable-dependency-tracking \
#     && make \
#     && make install

# RUN curl -o /root/bison-$BISON_VERSION.tar.xz -fSL \
#     http://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/bison/bison-$BISON_VERSION.tar.gz \
#     && curl -o /root/bison-$BISON_VERSION.tar.xz.sig -fSL \
#         http://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/bison/bison-$BISON_VERSION.tar.gz.sig \
#     && gpg --keyserver keys.gnupg.net --recv-keys 78D5264E \
#     && gpg --verify /root/bison-$BISON_VERSION.tar.xz.sig \
#     && cd /root \
#     && tar -xf bison-$BISON_VERSION.tar.xz \
#     && cd bison-$BISON_VERSION \
#     && ./configure --disable-dependency-tracking \
#     && make \
#     && make install

# RUN curl -o /root/flex-$FLEX_VERSION.tar.gz \
#     https://github.com/westes/flex/releases/download/v$FLEX_VERSION/flex-$FLEX_VERSION.tar.gz \
#     && cd /root \
#     && tar -xf flex-$FLEX_VERSION.tar.xz \
#     && cd flex-$FLEX_VERSION \
#     && ./configure \
#     && make \
#     && make install

WORKDIR /home
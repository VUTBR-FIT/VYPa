%{
#include "../src/parser.h"
#include "../src/global.h"

#include <string>
#include <iostream>
#include <fstream>

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int invalidToken;
extern int yylineno;
 
void yyerror(const char *s);
%}

%union {
	int ival;
	char* sval;
}

%token TOK_PLUS
%token TOK_MINUS
%token TOK_TIMES
%token TOK_DIVIDE
%token TOK_BRACKET_L
%token TOK_BRACKET_R
%token TOK_LESS
%token TOK_GREAT
%token TOK_COLON
%token TOK_SEMICOLON
%token TOK_COMMA
%token TOK_DOT
%token TOK_PARENT_BRACKET_L
%token TOK_PARENT_BRACKET_R
%token TOK_AND
%token TOK_OR
%token TOK_EQUAL
%token TOK_NOT_EQUAL
%token TOK_GREAT_EQUAL
%token TOK_LESS_EQUAL
%token TOK_NOT
%token TOK_ASSIGN

%token TOK_KW_VOID
%token TOK_KW_CLASS
%token TOK_KW_ELSE
%token TOK_KW_IF
%token TOK_KW_INT
%token TOK_KW_NEW
%token TOK_KW_RETURN
%token TOK_KW_STRING
%token TOK_KW_SUPER
%token TOK_KW_THIS
%token TOK_KW_WHILE

%token TOK_INVALID

%token <ival> TOK_INT
%token <sval> TOK_STRING
%token <sval> TOK_ID

%left TOK_OR
%left TOK_AND
%left TOK_EQUAL TOK_NOT_EQUAL
%left TOK_GREAT TOK_GREAT_EQUAL TOK_LESS TOK_LESS_EQUAL
%left TOK_PLUS TOK_MINUS
%left TOK_TIMES TOK_DIVIDE
%precedence TOK_NOT
%left TOK_DOT
%precedence TOK_BRACKET_L TOK_BRACKET_R
%precedence TOK_KW_NEW
%%


program:
	  function program
	| class program
	| { PARSER->EndOfProgram(); }
	;

function:
	  type TOK_ID TOK_BRACKET_L { PARSER->DefineFunction($2); } param_list block { PARSER->EndFunction(); }
	| TOK_KW_VOID { PARSER->SetType(TYPE_VOID); } TOK_ID TOK_BRACKET_L { PARSER->DefineFunction($3); } param_list block { PARSER->EndFunction(); }
	;

param_list:
	  TOK_KW_VOID TOK_BRACKET_R
	| type TOK_ID { PARSER->AddParam($2); } next_param
	;

next_param:
	  TOK_BRACKET_R
	| TOK_COMMA type TOK_ID { PARSER->AddParam($3); } next_param
	;

basic_type:
	  TOK_KW_INT { PARSER->SetType(TYPE_INT); }
	| TOK_KW_STRING { PARSER->SetType(TYPE_STRING); }
	;

type:
	  TOK_KW_INT { PARSER->SetType(TYPE_INT); }
	| TOK_KW_STRING { PARSER->SetType(TYPE_STRING); }
	| TOK_ID { PARSER->SetType($1); }
	;

block:
	  TOK_PARENT_BRACKET_L { PARSER->BeginBlock(); } commands TOK_PARENT_BRACKET_R { PARSER->EndBlock(); }
	;

commands:
	| var_def commands
	| TOK_KW_IF TOK_BRACKET_L expr { PARSER->If(); } TOK_BRACKET_R block TOK_KW_ELSE { PARSER->Else(); } block { PARSER->EndIf(); } commands
	| TOK_KW_WHILE { PARSER->BeginWhile(); } TOK_BRACKET_L expr TOK_BRACKET_R { PARSER->While(); } block { PARSER->EndWhile(); } commands
	| function_call TOK_SEMICOLON { PARSER->ExprUseless(); } commands
	| method_call TOK_SEMICOLON { PARSER->ExprUseless(); } commands
	| TOK_KW_RETURN expr TOK_SEMICOLON { PARSER->Return(true); } commands
	| TOK_KW_RETURN TOK_SEMICOLON { PARSER->Return(false); } commands
	| TOK_ID TOK_ASSIGN { PARSER->LValueAssign(false, $1); } expr TOK_SEMICOLON { PARSER->Assign(); } commands
	| expr TOK_DOT TOK_ID { PARSER->LValueAssign(true, $3); } TOK_ASSIGN expr TOK_SEMICOLON { PARSER->Assign(); } commands
	;

var_def:
	  type TOK_ID next_var { PARSER->DefineVariable($2); } 
	;

next_var:
	  TOK_SEMICOLON
	| TOK_COMMA TOK_ID { PARSER->DefineVariable($2); } next_var
	;

function_call:
	  TOK_ID TOK_BRACKET_L { PARSER->FunctionCall(false); } args { PARSER->FunctionCallEnd($1, false); }
	;

method_call:
	  expr TOK_DOT TOK_ID TOK_BRACKET_L { PARSER->FunctionCall(true); }  args { PARSER->FunctionCallEnd($3, true); }
	| TOK_KW_SUPER { PARSER->Super(); } TOK_DOT TOK_ID TOK_BRACKET_L { PARSER->FunctionCall(true); } args { PARSER->FunctionCallEnd($4, true); }
	;

args:
	  TOK_BRACKET_R
	| expr { PARSER->AddArg(); } next_arg
	;

next_arg:
	  TOK_BRACKET_R
	| TOK_COMMA expr { PARSER->AddArg(); } next_arg
	;

class:
	  TOK_KW_CLASS TOK_ID TOK_COLON TOK_ID TOK_PARENT_BRACKET_L { PARSER->DefineClass($2, $4); } defs TOK_PARENT_BRACKET_R { PARSER->EndClass(); }
	;

defs:
	| function defs
	| var_def TOK_SEMICOLON defs
	;

expr:
	  TOK_INT { PARSER->Int($1); }
	| TOK_STRING { PARSER->String($1); }
	| TOK_ID { PARSER->Id($1); }
	| function_call
	| method_call
	| expr TOK_DOT TOK_ID { PARSER->ObjectVar($3); }
	| TOK_KW_NEW TOK_ID { PARSER->ObjectCreation($2); }
	| TOK_KW_THIS { PARSER->This(); }
	| TOK_BRACKET_L basic_type TOK_BRACKET_R { PARSER->CastingType(); } expr { PARSER->Cast(); }

	| TOK_BRACKET_L parleft expr TOK_BRACKET_R parright
	| TOK_BRACKET_L parleft expr TOK_BRACKET_R parright expr { PARSER->Cast(); }
	| TOK_NOT expr { PARSER->Not(); }
	| expr TOK_TIMES expr { PARSER->Times(); }
	| expr TOK_DIVIDE expr { PARSER->Divide(); }
	| expr TOK_PLUS expr { PARSER->Plus(); }
	| expr TOK_MINUS expr { PARSER->Minus(); }
	| expr TOK_LESS expr { PARSER->Less(); }
	| expr TOK_LESS_EQUAL expr { PARSER->Lessequal(); }
	| expr TOK_GREAT expr { PARSER->Great(); }
	| expr TOK_GREAT_EQUAL expr { PARSER->Greatequal(); }
	| expr TOK_EQUAL expr { PARSER->Equal(); }
	| expr TOK_NOT_EQUAL expr { PARSER->Notequal(); }
	| expr TOK_AND expr { PARSER->And(); }
	| expr TOK_OR expr { PARSER->Or(); }
	;

parleft:
	  { PARSER->ParLeft(); }
	;

parright:
	  { PARSER->ParRight(); }
	;

%%

int main(int argc, char** argv)
{
	try
	{
		if (argc != 2 && argc != 3)
		{
			std::cerr << "Compiler requiers one or two arguments" << std::endl;
			return 9;
		}
		FILE* in = fopen(argv[1], "r");
		if (in == NULL)
		{
			std::cerr << "Error while opening input file" << std::endl;
				return 9;
		}
		if (argc == 3)
		{
			if (!PARSER->SetOutput(argv[2]))
			{
				return 9;
			}
		}
		yyin = in;
		while (!feof(yyin))
		{
			yyparse();
		}
		PARSER->Cleanup();
	}
	catch(ExceptionVYPa& ex)
	{
		PARSER->Cleanup();
		switch(ex.GetError())
		{
			case ERROR_LEXICAL:
				std::cerr << "Lexical";
				break;
			case ERROR_SYNTAX:
				std::cerr << "Syntax";
				break;
			case ERROR_TYPE:
				std::cerr << "Type";
				break;
			case ERROR_SEMANTIC:
				std::cerr << "Semantic";
				break;
			case ERROR_CODEGEN:
				std::cerr << "Code generator";
				break;
			case ERROR_INTERNAL:
				std::cerr << "Internal";
				break;
			default:
				std::cerr << "OK";
				break;
		}
		std::cerr << " error ";
		if (ex.GetLine())
		{
			std::cerr << "(line: " << yylineno << ") ";
		}
		std::cerr << ": " << ex.what() << std::endl;
		return ex.GetError();
	}
	catch(std::exception& ex)
	{
		PARSER->Cleanup();
		std::cerr << "Internal error : " << ex.what();
		return ERROR_INTERNAL;
	}
}

void yyerror(UNUSED const char *s) {
	if (invalidToken)
	{
		throw ExceptionVYPa(ERROR_LEXICAL, "Unknown token", true);
	}
	else
	{
		throw ExceptionVYPa(ERROR_SYNTAX, "No rule matches the input file", true);
	}
}

#ifndef H_EXPR_ANALYZER
#define H_EXPR_ANALYZER

#include "global.h"
#include "symbol_table.h"

enum NodeType
{
	NODE_USELESS,
	NODE_TEST,
	NODE_TYPE,
	NODE_VARIABLE,
	NODE_CONST,
	NODE_FUNCTION,
	NODE_ASSIGNMENT,
	NODE_RETURN,
	NODE_NEW,
	NODE_CAST,
	NODE_NOT,
	NODE_MUL,
	NODE_DIV,
	NODE_ADD,
	NODE_SUB,
	NODE_LES,
	NODE_LEQ,
	NODE_GRT,
	NODE_GEQ,
	NODE_EQ,
	NODE_NEQ,
	NODE_AND,
	NODE_OR,
	NODE_OBJ_VARIABLE,
	NODE_OBJ_CREATION,
	NODE_OBJ_KEYWORD
};

class ExprNode;

class ExprStack
{
public:
	ExprStack(unsigned insIndex);
	~ExprStack();
	unsigned Evaluate(unsigned insOffset);
	void AddNode(ExprNode* node);
	ExprNode* GetNode(unsigned index);
	Function* GetFunction();
	unsigned& GetSP();

	void Print();
private:
	std::vector<ExprNode*> m_Nodes;
	unsigned m_InsIndex;
	unsigned m_StackPointer;
};

class ExprAnalyzer
{
public:
	~ExprAnalyzer();
	void Evaluate();
	void AddStack(ExprStack* stack);

	void Print();
private:
	std::vector<ExprStack*> m_Stack;
};

class ExprNode
{
public:
	ExprNode(NodeType type);
	virtual ~ExprNode();
	Type& GetType();
	NodeType& GetNodeType();
	virtual unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) = 0;
	virtual void Print() = 0;
protected:
	NodeType m_NodeType;
	Type m_DataType;
};

class ExprNodeObjectKeyword : public ExprNode
{
public:
	ExprNodeObjectKeyword(std::string cls, bool super);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	void Print() override;

	bool IsSuper();
private:
	bool m_Super;
};

class ExprNodeObjectCreation : public ExprNode
{
public:
	ExprNodeObjectCreation(std::string name);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	void Print() override;
private:
	void CallConstructor(unsigned SP, unsigned* insOffset, Class* cls);
	std::string m_Name;
};

class ExprNodeObjectVariable : public ExprNode
{
public:
	ExprNodeObjectVariable(std::string name, bool assign);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	void Print() override;

	unsigned& GetIndex();
private:
	std::string m_Name;
	bool m_Assign;
	unsigned m_Index;
};

class ExprNodeReturn : public ExprNode
{
public:
	ExprNodeReturn(Type type);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	void Print() override;
};


class ExprNodeOperation1 : public ExprNode
{
public:
	ExprNodeOperation1(NodeType type);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	void Print() override;
private:
	void CheckIO(Type a, std::string op);
};

class ExprNodeOperation2 : public ExprNode
{
public:
	ExprNodeOperation2(NodeType type);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	void Print() override;
private:
	void CheckI(Type a, Type b, std::string op);
	void CheckIS(Type a, Type b, std::string op);
	void CheckIO(Type a, Type b, std::string op);
	void CheckISO(Type a, Type b, std::string op);
	void CheckAssignment(Type a, Type b);
	void CheckCast(Type a, Type b);
	void CleanAfterStrings(unsigned SP, unsigned* insOffset, bool copyToStack = true);
};

class ExprNodeType : public ExprNode
{
public:
	ExprNodeType(Type type);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;

	void Print() override;
};


class ExprNodeVariable : public ExprNode
{
public:
	ExprNodeVariable(std::string name, unsigned index, Type type, bool assign = false);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;
	unsigned& GetIndex();

	void Print() override;
private:
	std::string m_Name;
	unsigned m_Index;
	bool m_Assign;
};

class ExprNodeConst : public ExprNode
{
public:
	ExprNodeConst(std::string value);
	ExprNodeConst(int value);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;

	void Print() override;
private:
	std::string m_ValueStr;
	int m_ValueInt;
};

class ExprNodeFunction : public ExprNode
{
public:
	ExprNodeFunction(std::string name, unsigned params, bool method);
	unsigned Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset) override;

	void Print() override;
private:
	void CheckTypes(Type a, Type b, unsigned index);

	std::string m_Name;
	unsigned m_Params;
	bool m_Method;
};

#endif

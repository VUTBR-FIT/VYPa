#ifndef H_GLOBAL
#define H_GLOBAL

#include <exception>
#include <string>
#include <cassert>

enum ErrorCodes
{
	ERROR_OK = 0,
	ERROR_LEXICAL = 1,
	ERROR_SYNTAX = 2,
	ERROR_TYPE = 3,
	ERROR_SEMANTIC = 4,
	ERROR_CODEGEN = 5,
	ERROR_INTERNAL = 9
};

enum Registers
{
	REG_ACC,
	REG_BP,
	REG_TMP,
	REG_3,
	REG_4,
	REG_5,
	REG_6,
	REG_CHUNK,
	REG_PC,
	REG_SP
};

enum ValType
{
	VAL_LABEL,
	VAL_REG,
	VAL_REG_ADR,
	VAL_STACK,
	VAL_BASE,
	VAL_IMID
};

enum Opcode
{
	OP_LABEL,
	OP_CALL,
	OP_RET,
	OP_JUMPZ,
	OP_JUMPNZ,
	OP_JUMP,
	OP_SET,
	OP_ADDI,
	OP_SUBI,
	OP_DIVI,
	OP_MULI,
	OP_LTI,
	OP_GTI,
	OP_EQI,
	OP_LTS,
	OP_GTS,
	OP_EQS,
	OP_NOT,
	OP_AND,
	OP_OR,
	OP_INT2STRING,
	OP_ADDS,
	OP_COPY,
	OP_DESTROY,
	OP_CREATE,
	OP_GETWORD,
	OP_SETWORD,
	OP_COMMENT,
	OP_WRITES,
	OP_READINT,
	OP_READSTRING,
	OP_GETSIZE
};

enum BasicTypes
{
	TYPE_VOID,
	TYPE_INT,
	TYPE_STRING,
	TYPE_OBJECT,
	TYPE_UNKNOWN
};

#define LABEL_UNKNOWN -1
#define PARAM_INF -1

#define UNUSED __attribute__((unused))

#define ASSERT_PTR(x) assert((x)!=nullptr)
#define ASSERT_NULL(x) assert((x)==nullptr)
#define ASSERT_EMPTY(x) assert(!(x).empty())
#define ASSERT_NEGATIVE(x) assert((x)>=0)
#define ASSERT_NOT_EMPTY(x) assert((x).empty())

#define PARSER Parser::GetParser()

class ExceptionVYPa : public std::exception
{
public:
	ExceptionVYPa (ErrorCodes error, std::string message, bool line = false)
		: m_Message(message),
		  m_Error(error),
		  m_Line(line)
	{}

	const char* what() const throw()
	{
		return m_Message.c_str();
	}

	ErrorCodes GetError()
	{
		return m_Error;
	}

	bool GetLine()
	{
		return m_Line;
	}

private:
	std::string m_Message;
	ErrorCodes m_Error;
	bool m_Line;
};

#endif

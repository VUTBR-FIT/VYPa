#ifndef H_SYMBOL_TABLE
#define H_SYMBOL_TABLE

#include "global.h"
#include <string>
#include <vector>

class Type;
class Variable;
class Function;
class Class;

class Type
{
public:
	Type(BasicTypes type, std::string specifier = "");
	BasicTypes& GetType();
	std::string& GetSpecifier();

	std::string GetText();

private:
	BasicTypes m_Type;
	std::string m_Specifier;
};

class Variable
{
public:
	Variable(std::string name, Type type, unsigned block = 0);

	std::string& GetName();
	Type& GetType();
	unsigned& GetBlock();
	void SetUsed();
	bool& GetUsed();

	void Print(unsigned tabs);

private:
	std::string m_Name;
	Type m_Type;
	unsigned m_Block;
	bool m_Used;
};

class Function
{
public:
	Function(std::string name, Type type, Class* object, bool buildIn = false);
	~Function();
	void AddParam(Variable* var);
	unsigned AddVariable(Variable* var);
	void SetInfiniteParams();
	void CleanVariables(unsigned block);
	void SetUsed();

	std::string& GetName();
	Type& GetType();
	Variable* FindVariable(std::string name, unsigned* index = nullptr);
	Variable* GetVariable(unsigned index);
	int& GetParamCount();
	std::string GetUniqueName();
	Class* GetObject();
	unsigned& GetMaxVars();
	bool& GetUsed();
	void Evaluate();
	bool IsBuildIn();

	void Print(unsigned tabs);
private:
	std::string m_Name;
	Type m_Type;
	Class *m_Object;
	std::vector<Variable*> m_Variables;
	int m_ParamCount;
	unsigned m_MaxVars;
	std::vector<std::string> m_Names;
	bool m_Used;
	bool m_BuildIn;
};

class Class
{
public:
	Class(std::string name, std::string superior, bool buildIn = false);
	~Class();
	void AddFunction(Function* fun);
	void AddVariable(Variable* var);
	void SetParent(Class* cls);

	std::string& GetName();
	std::string& GetSuperior();
	Class* GetParent();
	unsigned GetVariablesCount();
	Function* GetConstructor();
	Variable* FindVariable(std::string name, unsigned* index = nullptr);
	Variable* GetVariable(unsigned index);
	Function* FindFunction(std::string name);
	void Evaluate();
	void CheckUsage();
	bool IsBuildIn();

	void Print(unsigned tabs);

private:
	std::string m_Name;
	std::string m_Superior;
	Class* m_Parent;
	Function* m_Constructor;
	std::vector<Function*> m_Functions;
	std::vector<Variable*> m_Variables;
	bool m_BuildIn;
};

class SymbolTable
{

public:
	~SymbolTable();
	void AddFunction(Function* fun);
	void AddClass(Class* object);
	void Evaluate();
	void CheckUsage();
	Function* GetMain();

	Function* FindFunction(std::string fun);
	Class* FindClass(std::string cls);
	bool IsSubclass(std::string cls1, std::string cls2);

	void Print();

private:
	std::vector<Function*> m_Functions;
	std::vector<Class*> m_Classes;
	Function* m_Main;
};

#endif

#ifndef H_INSTRUCTION_TAPE
#define H_INSTRUCTION_TAPE

#include "global.h"

#include <string>
#include <vector>

class Value
{
public:
	Value(ValType type, int value = 0);
	Value(ValType type, std::string value);

	std::string Print();
	bool IsString();
	ValType GetType();
private:
	bool m_IsString;
	ValType m_Type;
	std::string m_String;
	int m_Integer;
};

class Instruction
{
public:
	Instruction(Opcode opcode, Value* dst = nullptr, Value* src1 = nullptr, Value* src2 = nullptr);

	std::string Print();
	bool HaveString();
	Opcode GetType();
	Value *GetSrc1();
	Value *GetSrc2();
	Value *GetDst();

private:
	Opcode m_Opcode;
	Value* m_Dst;
	Value* m_Src1;
	Value* m_Src2;
};

class InstructionTape
{
public:
	InstructionTape();
	~InstructionTape();
	void AddInstruction(Instruction* ins);
	void AddInstruction(Instruction* ins, unsigned index);
	unsigned GetCurrentIndex();
	std::string Print();
	void Evaluate();

private:
	void GeneratePrint();
	void GenerateReadInt();
	void GenerateReadString();
	void GenerateLength();
	void GenerateSubStr();
	void GenerateFunctionReturn();

	std::vector<Instruction*> m_Instructions;
};

#endif

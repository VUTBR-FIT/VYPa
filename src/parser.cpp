#include "parser.h"
#include "instruction_tape.h"

#include <cstring>
#include <iostream>
#include <sstream>

#if defined(DEBUG_LEX) || defined(DEBUG_SYNTAX)
	#define DEBUG_SKIP return
    #define DEBUG_SYM false
#else
	#ifdef DEBUG_SYM_TABLE
		#define DEBUG_SYM true
    #else
		#define DEBUG_SYM false
	#endif
	#define DEBUG_SKIP
#endif

Parser* Parser::m_Singleton = nullptr;

Parser::Parser()
	: m_JumpCounter(0),
	  m_LastType (Type(TYPE_UNKNOWN)),
	  m_Output(nullptr),
	  m_Function(nullptr),
	  m_Class(nullptr),
	  m_SymbolTable(new SymbolTable()),
	  m_InstructionTape(new InstructionTape()),
	  m_AssignIndex(0),
	  m_Expr(nullptr),
	  m_ExprAnalyzer(new ExprAnalyzer()),
	  m_Block(0)
{
	Variable* var = nullptr;

	SetType(TYPE_VOID);
	m_Function = new Function("print", m_LastType, nullptr, true);
	m_Function->SetInfiniteParams();
	m_SymbolTable->AddFunction(m_Function);
	m_Function = nullptr;

	SetType(TYPE_INT);
	m_Function = new Function("readInt", m_LastType, nullptr, true);
	m_SymbolTable->AddFunction(m_Function);
	m_Function = nullptr;

	SetType(TYPE_STRING);
	m_Function = new Function("readString", m_LastType, nullptr, true);
	m_SymbolTable->AddFunction(m_Function);
	m_Function = nullptr;

	SetType(TYPE_INT);
	m_Function = new Function("length", m_LastType, nullptr, true);
	SetType(TYPE_STRING);
	var = new Variable("s", m_LastType);
	m_Function->AddParam(var);
	m_SymbolTable->AddFunction(m_Function);
	m_Function = nullptr;

	SetType(TYPE_STRING);
	m_Function = new Function("subStr", m_LastType, nullptr, true);
	SetType(TYPE_STRING);
	var = new Variable("s", m_LastType);
	m_Function->AddParam(var);
	SetType(TYPE_INT);
	var = new Variable("i", m_LastType);
	m_Function->AddParam(var);
	SetType(TYPE_INT);
	var = new Variable("n", m_LastType);
	m_Function->AddParam(var);
	m_SymbolTable->AddFunction(m_Function);
	m_Function = nullptr;

	m_Class = new Class("Object", "", true);
	SetType(TYPE_STRING);
	m_Function = new Function("toString", m_LastType, m_Class, true);
	m_Class->AddFunction(m_Function);
	m_Function = nullptr;
	SetType(TYPE_STRING);
	m_Function = new Function("getClass", m_LastType, m_Class, true);
	m_Class->AddFunction(m_Function);
	m_Function = nullptr;
	m_SymbolTable->AddClass(m_Class);
	m_Class = nullptr;
}

Parser* Parser::GetParser()
{
	if (m_Singleton == nullptr)
	{
		m_Singleton = new Parser;
	}
	return m_Singleton;
}

SymbolTable* Parser::GetSymbolTable()
{
	return m_SymbolTable;
}

InstructionTape* Parser::GetInstructionTape()
{
	return m_InstructionTape;
}

ExprAnalyzer* Parser::GetExprAnalyzer()
{
	return m_ExprAnalyzer;
}

void Parser::Cleanup()
{
	if (m_Output.is_open())
	{
		m_Output.close();
	}
}

bool Parser::SetOutput(char* file)
{
	m_Output.open(file, std::ios::out | std::ios::trunc);
	if (!m_Output.is_open())
	{
		std::cerr << "Error while opening output file" << std::endl;
		return false;
	}
	return true;
}

void Parser::EndOfProgram()
{
	DEBUG_SKIP;

	ASSERT_NULL(m_Function);
	ASSERT_NULL(m_Class);
	ASSERT_NULL(m_Expr);

	if (DEBUG_SYM)
	{
		m_SymbolTable->Print();
		m_ExprAnalyzer->Print();
		return;
	}

	m_SymbolTable->Evaluate();
	m_ExprAnalyzer->Evaluate();
	m_SymbolTable->CheckUsage();
	m_InstructionTape->Evaluate();

	std::cerr << "Program parsing completed successfully" << std::endl;

	if (m_Output.is_open())
	{
		m_Output << m_InstructionTape->Print();
	}
	else
	{
		std::cout << m_InstructionTape->Print();
	}
}

void Parser::SetType(BasicTypes type)
{
	DEBUG_SKIP;

	m_LastType = Type(type);
}

void Parser::SetType(char* type)
{
	DEBUG_SKIP;
	ASSERT_PTR(type);

	m_LastType = Type(TYPE_OBJECT, type);
}

void Parser::DefineFunction(char* name)
{
	DEBUG_SKIP;
	ASSERT_PTR(name);

	std::string object = "";
	if (m_Class != nullptr)
	{
		object = m_Class->GetName();
	}
	m_Function = new Function(std::string(name), m_LastType, m_Class);
	m_InstructionTape->AddInstruction(new Instruction(Opcode::OP_LABEL, new Value(ValType::VAL_LABEL, m_Function->GetUniqueName())));
}

void Parser::AddParam(char* name)
{
	DEBUG_SKIP;
	ASSERT_PTR(name);
	ASSERT_PTR(m_Function);

	Variable* var = new Variable(name, m_LastType, m_Block);
	m_Function->AddParam(var);
}

void Parser::EndFunction()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_Function);

	Value* dst;
	Value* src1;
	Instruction* ins;
	
	dst = new Value(VAL_REG, REG_ACC);
	if (m_Function->GetType().GetType() == TYPE_STRING)
	{
		src1 = new Value(VAL_IMID, "\"\"");
	}
	else
	{
		src1 = new Value(VAL_IMID, 0);
	}
	ins = new Instruction(OP_SET, dst, src1);
	m_InstructionTape->AddInstruction(ins);

	Return(false);

	if (m_Class == nullptr)
	{
		m_SymbolTable->AddFunction(m_Function);
	}
	else
	{
		m_Class->AddFunction(m_Function);
	}
	m_Function = nullptr;
}

void Parser::DefineClass(char* name, char* superior)
{
	DEBUG_SKIP;
	ASSERT_PTR(name);
	ASSERT_PTR(superior);

	m_Class = new Class(name, superior);
}

void Parser::EndClass()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_Class);

	m_SymbolTable->AddClass(m_Class);
	m_Class = nullptr;
}

void Parser::DefineVariable(char* name)
{
	DEBUG_SKIP;
	ASSERT_PTR(name);

	Variable* var = new Variable(name, m_LastType, m_Block);
	if (m_Function == nullptr)
	{
		ASSERT_PTR(m_Class);
		m_Class->AddVariable(var);
	}
	else
	{
		ASSERT_PTR(m_Function);
		unsigned index = m_Function->AddVariable(var);
		Value* dst = new Value(VAL_BASE, index);
		Value* src1;
		if (m_LastType.GetType() == TYPE_STRING)
		{
			src1 = new Value(VAL_IMID, "\"\"");
		}
		else
		{
			src1 = new Value(VAL_IMID, 0);
		}
		m_InstructionTape->AddInstruction(new Instruction(OP_SET, dst, src1));
	}
}

void Parser::Return(bool expr)
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);
	ASSERT_PTR(m_Function);
	
	if (expr)
	{
		StartExpr();
		m_Expr->AddNode(new ExprNodeReturn(m_Function->GetType()));
		EndExpr();
	}

	Value* dst;
	Value* val;

	dst = new Value(VAL_REG, REG_TMP);
	val = new Value(VAL_STACK, -1);
	m_InstructionTape->AddInstruction(new Instruction(OP_SET, dst, val));
	dst = new Value(VAL_REG, REG_BP);
	val = new Value(VAL_STACK, -3);
	m_InstructionTape->AddInstruction(new Instruction(OP_SET, dst, val));
	dst = new Value(VAL_REG, REG_SP);
	val = new Value(VAL_STACK, -2);
	m_InstructionTape->AddInstruction(new Instruction(OP_SET, dst, val));
	val = new Value(VAL_REG, REG_TMP);
	m_InstructionTape->AddInstruction(new Instruction(OP_RET, val));
}

void Parser::If()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation1(NODE_TEST));
	EndExpr();

	m_JumpCounter++;
	m_JumpStack.push_back(m_JumpCounter);
	std::stringstream ss;
       	ss << "if." << m_JumpStack.back() << ".a";
	Value* label = new Value(VAL_LABEL, ss.str());
	Value* condition = new Value(VAL_REG, REG_ACC);
	m_InstructionTape->AddInstruction(new Instruction(OP_JUMPZ, label, condition));
}

void Parser::Else()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);
	ASSERT_EMPTY(m_JumpStack);

	std::stringstream ss;
	ss << "if." << m_JumpStack.back() << ".b";
	Value* label = new Value(VAL_LABEL, ss.str());
	m_InstructionTape->AddInstruction(new Instruction(OP_JUMP, label));
	ss.str("");
	ss << "if." << m_JumpStack.back() << ".a";
	label = new Value(VAL_LABEL, ss.str());
	m_InstructionTape->AddInstruction(new Instruction(OP_LABEL, label));
}

void Parser::EndIf()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);
	ASSERT_EMPTY(m_JumpStack);

	std::stringstream ss;
	ss << "if." << m_JumpStack.back() << ".b";
	m_JumpStack.pop_back();
	Value* label = new Value(VAL_LABEL, ss.str());
	m_InstructionTape->AddInstruction(new Instruction(OP_LABEL, label));
}

void Parser::BeginWhile()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);

	m_JumpCounter++;
	m_JumpStack.push_back(m_JumpCounter);
	std::stringstream ss;
	ss << "while." << m_JumpStack.back() << ".a";
	Value* label = new Value(VAL_LABEL, ss.str());
	m_InstructionTape->AddInstruction(new Instruction(OP_LABEL, label));

}

void Parser::While()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);
	ASSERT_EMPTY(m_JumpStack);
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation1(NODE_TEST));
	EndExpr();

	std::stringstream ss;
	ss << "while." << m_JumpStack.back() << ".b";
	Value* label = new Value(VAL_LABEL, ss.str());
	Value* condition = new Value(VAL_REG, REG_ACC);
	m_InstructionTape->AddInstruction(new Instruction(OP_JUMPZ, label, condition));
}

void Parser::EndWhile()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_InstructionTape);
	ASSERT_EMPTY(m_JumpStack);

	std::stringstream ss;
	ss << "while." << m_JumpStack.back() << ".a";
	Value* label = new Value(VAL_LABEL, ss.str());
	m_InstructionTape->AddInstruction(new Instruction(OP_JUMP, label));
	ss.clear();
	ss << "while." << m_JumpStack.back() << ".b";
	label = new Value(VAL_LABEL, ss.str());
	m_InstructionTape->AddInstruction(new Instruction(OP_LABEL, label));
}

void Parser::Assign()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_ASSIGNMENT));

	EndExpr();
}

void Parser::LValueAssign(bool object, char* name)
{
	DEBUG_SKIP;
	ASSERT_PTR(name);
	StartExpr();

	std::string nameStr = std::string(name);
	unsigned index;

	if (object)
	{
		m_Expr->AddNode(new ExprNodeObjectVariable(nameStr, true));
	}
	else
	{
		ASSERT_PTR(m_Function);
		Variable* var = m_Function->FindVariable(nameStr, &index);
		m_Expr->AddNode(new ExprNodeVariable(nameStr, index, var->GetType(), true));
		var->SetUsed();
	}
}

void Parser::FunctionCall(bool method)
{
	DEBUG_SKIP;
	StartExpr();

	m_ParamCounts.push_back(0);

	if (method)
	{
		AddArg();
	}
}

void Parser::FunctionCallEnd(char* name, bool method)
{
	DEBUG_SKIP;
	ASSERT_EMPTY(m_ParamCounts);
	StartExpr();

	m_Expr->AddNode(new ExprNodeFunction(std::string(name), m_ParamCounts.back(), method));
	m_ParamCounts.pop_back();
}

void Parser::AddArg()
{
	DEBUG_SKIP;
	ASSERT_EMPTY(m_ParamCounts);

	m_ParamCounts.back()++;
}

void Parser::Int(int value)
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeConst(value));
}

void Parser::String(char* value)
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeConst(std::string(value)));
}

void Parser::Id(char* name)
{
	DEBUG_SKIP;
	StartExpr();

	std::string strName = std::string(name);
	unsigned index;
	Variable* var = m_Function->FindVariable(strName, &index);
	m_Expr->AddNode(new ExprNodeVariable(name, index, var->GetType()));
}

void Parser::ObjectVar(UNUSED char* name)
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeObjectVariable(std::string(name), false));
}

void Parser::ObjectCreation(UNUSED char* name)
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeObjectCreation(std::string(name)));
}

void Parser::This()
{
	DEBUG_SKIP;
	if (m_Class == nullptr)
	{
		throw ExceptionVYPa(ERROR_SEMANTIC, "Keyword 'this' was used outside of class", true);
	}
	m_Expr->AddNode(new ExprNodeObjectKeyword(m_Class->GetName(), false));

}

void Parser::Super()
{
	DEBUG_SKIP;
	StartExpr();

	if (m_Class == nullptr)
	{
		throw ExceptionVYPa(ERROR_SEMANTIC, "Keyword 'super' was used outside of class", true);
	}
	m_Expr->AddNode(new ExprNodeObjectKeyword(m_Class->GetName(), true));
}

void Parser::CastingType()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeType(m_LastType));
}

void Parser::Cast()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_CAST));
}

void Parser::Not()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation1(NODE_NOT));
}

void Parser::Times()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_MUL));
}

void Parser::Divide()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_DIV));
}

void Parser::Plus()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_ADD));
}

void Parser::Minus()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_SUB));
}

void Parser::Less()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_LES));
}

void Parser::Lessequal()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_LEQ));
}

void Parser::Great()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_GRT));
}

void Parser::Greatequal()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_GEQ));
}

void Parser::Equal()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_EQ));
}

void Parser::Notequal()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_NEQ));
}

void Parser::And()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_OR));
}

void Parser::Or()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation2(NODE_OR));
}

void Parser::ParLeft()
{
	DEBUG_SKIP;
	// TODO
}

void Parser::ParRight()
{
	DEBUG_SKIP;
	// TODO
}

void Parser::ExprUseless()
{
	DEBUG_SKIP;
	StartExpr();

	m_Expr->AddNode(new ExprNodeOperation1(NODE_USELESS));
	EndExpr();
}

void Parser::EndExpr()
{
	DEBUG_SKIP;
	ASSERT_PTR(m_Expr);

	m_ExprAnalyzer->AddStack(m_Expr);
	m_Expr = nullptr;
}

void Parser::StartExpr()
{
	DEBUG_SKIP;

	if (m_Expr == nullptr)
	{
		m_Expr = new ExprStack(m_InstructionTape->GetCurrentIndex());
	}
}

void Parser::BeginBlock()
{
	DEBUG_SKIP;

	m_Block++;
}

void Parser::EndBlock()
{
	DEBUG_SKIP;
	assert(m_Block>0);
	ASSERT_PTR(m_Function);

	m_Block--;
	m_Function->CleanVariables(m_Block);
}

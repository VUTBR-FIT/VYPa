#include "expr_analyzer.h"
#include "parser.h"
#include "instruction_tape.h"

#include <iostream>
#include <sstream>

void AddIns(Instruction* ins, unsigned* index)
{
	PARSER->GetInstructionTape()->AddInstruction(ins, (*index)++);
}

ExprNodeObjectKeyword::ExprNodeObjectKeyword(std::string cls, bool super)
	: ExprNode(NODE_OBJ_KEYWORD),
	  m_Super(super)
{
	m_DataType = Type(TYPE_OBJECT, cls);
}

unsigned ExprNodeObjectKeyword::Evaluate(ExprStack* stack, UNUSED unsigned position, unsigned* insOffset)
{
	Value* dst;
	Value* src1;
	Instruction* ins;

	dst = new Value(VAL_STACK, (stack->GetSP())++);
	src1 = new Value(VAL_BASE, 0);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);

	return 0;
}

void ExprNodeObjectKeyword::Print()
{
	std::cerr << (m_Super ? "SUPER" : " THIS");
}

bool ExprNodeObjectKeyword::IsSuper()
{
	return m_Super;
}

ExprNodeObjectCreation::ExprNodeObjectCreation(std::string name)
	: ExprNode(NODE_OBJ_CREATION),
	  m_Name(name)
{
	m_DataType = Type(TYPE_OBJECT, name);
}

void ExprNodeObjectCreation::CallConstructor(unsigned SP, unsigned* insOffset, Class* cls)
{
	if (cls->GetParent() != nullptr)
	{
		CallConstructor(SP, insOffset, cls->GetParent());
	}

	Function* fun = cls->GetConstructor();
	if (fun == nullptr)
	{
		return;
	}

	Value* dst;
	Value* src1;
	Value* src2;
	Instruction* ins;

	unsigned vars = fun->GetMaxVars();

	dst = new Value(VAL_STACK, SP+vars);
	src1 = new Value(VAL_REG, REG_BP);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);
	dst = new Value(VAL_REG, REG_BP);
	src1 = new Value(VAL_REG, REG_SP);
	src2 = new Value(VAL_IMID, SP);
	ins = new Instruction(OP_ADDI, dst, src1, src2);
	AddIns(ins, insOffset);

	dst = new Value(VAL_STACK, SP+vars+1);
	src1 = new Value(VAL_REG, REG_SP);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);
	dst = new Value(VAL_REG, REG_SP);
	src1 = new Value(VAL_REG, REG_BP);
	src2 = new Value(VAL_IMID, vars + 3);
	ins = new Instruction(OP_ADDI, dst, src1, src2);
	AddIns(ins, insOffset);

	dst = new Value(VAL_STACK, SP+vars+2);
	src1 = new Value(VAL_LABEL, fun->GetUniqueName());
	ins = new Instruction(OP_CALL, dst, src1);
	AddIns(ins, insOffset);
}

unsigned ExprNodeObjectCreation::Evaluate(ExprStack* stack, UNUSED unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* src2;
	Value* dst;
	Instruction* ins;

	Class* cls = PARSER->GetSymbolTable()->FindClass(m_Name);
	dst = new Value(VAL_STACK, (stack->GetSP())++);
	src1 = new Value(VAL_IMID, cls->GetVariablesCount());
	ins = new Instruction(OP_CREATE, dst, src1);

	for (unsigned i = 0; i < cls->GetVariablesCount(); i++)
	{
		dst = new Value(VAL_STACK, stack->GetSP()-1);
		src1 = new Value(VAL_IMID, i);
		if (cls->GetVariable(i)->GetType().GetType() == TYPE_STRING)
		{
			src2 = new Value(VAL_IMID, "\"\"");
		}
		else
		{
			src2 = new Value(VAL_IMID, 0);
		}
		ins = new Instruction(OP_SETWORD, dst, src1, src2);
		AddIns(ins, insOffset);
	}

	CallConstructor(stack->GetSP(), insOffset, cls);

	return 0;
}

void ExprNodeObjectCreation::Print()
{
	std::cerr << "OBJECT CREATION: " << m_Name;
}

ExprNodeObjectVariable::ExprNodeObjectVariable(std::string name, bool assign)
	: ExprNode(NODE_OBJ_VARIABLE),
	  m_Name(name),
	  m_Assign(assign),
	  m_Index(-1)
{}

unsigned ExprNodeObjectVariable::Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* src2;
	Value* dst;
	Instruction* ins;

	assert(position >= 1);
	Type obj = stack->GetNode(position-1)->GetType();

	if (obj.GetType() != TYPE_OBJECT)
	{
		std::stringstream ss;
		ss << "Operator '.' expects operant of type 'object', but operant of type '" << obj.GetText() << "'was found";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
	
	Class* cls = PARSER->GetSymbolTable()->FindClass(obj.GetSpecifier());
	Variable* var = cls->FindVariable(m_Name, &m_Index);
	m_DataType = var->GetType();
	var->SetUsed();

	if (!m_Assign)
	{
		src1 = new Value(VAL_STACK, stack->GetSP()-1);
		src2 = new Value(VAL_IMID, m_Index);
		dst = new Value(VAL_STACK, stack->GetSP()-1);
		ins = new Instruction(OP_GETWORD, dst, src1, src2);
		AddIns(ins, insOffset);
	}

	return 1;
}

void ExprNodeObjectVariable::Print()
{
	std::cerr << "OBJECT VARIABLE: " << m_Name;
}

unsigned& ExprNodeObjectVariable::GetIndex()
{
	return m_Index;
}

ExprNodeReturn::ExprNodeReturn(Type type)
	: ExprNode(NODE_RETURN)
{
	m_DataType = type;
}

unsigned ExprNodeReturn::Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* dst;
	Instruction* ins;

	assert(position >= 1);
	Type a = stack->GetNode(position-1)->GetType();

	if (a.GetType() != m_DataType.GetType())
	{
		std::stringstream ss;
		ss << "Operator 'return' expects operants of the same type 'int', 'string' or 'object', but operants are of types '" << a.GetText() << "' and '" << m_DataType.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
	if (a.GetType() == TYPE_OBJECT && !(PARSER->GetSymbolTable()->IsSubclass(m_DataType.GetSpecifier(), a.GetSpecifier())))
	{
		std::stringstream ss;
		ss << "Operator 'return' expects object to be a subclass of function return type, but '" << a.GetText() << "' is not a subclass of '" << m_DataType.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}

	src1 = new Value(VAL_STACK, --(stack->GetSP()));
	dst = new Value(VAL_REG, REG_ACC);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);

	return 1;
}

void ExprNodeReturn::Print()
{
	std::cerr << "RETURN: " << m_DataType.GetText();
}

ExprNodeOperation1::ExprNodeOperation1(NodeType type)
	: ExprNode(type)
{}

void ExprNodeOperation1::CheckIO(Type a, std::string op)
{
	if (a.GetType() == TYPE_STRING)
	{
		std::stringstream ss;
		ss << "Operator '" << op << "' expects operant of type 'int' or 'object', but operant is of type '" << a.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

unsigned ExprNodeOperation1::Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* dst;
	Instruction* ins;
	
	assert(position >= 1);
	Type a = stack->GetNode(position-1)->GetType();

	switch (m_NodeType)
	{
		case NODE_NOT:
			CheckIO(a, "!");
			m_DataType = Type(TYPE_INT);
			src1 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_STACK, stack->GetSP()-1);
			ins = new Instruction(OP_NOT, dst, src1);
			AddIns(ins, insOffset);
			break;
		case NODE_USELESS:
			assert(position==1);
			if (a.GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_STACK, stack->GetSP()-1);
				ins = new Instruction(OP_DESTROY, dst);
				AddIns(ins, insOffset);
			}
			stack->GetSP()--;
			break;
		case NODE_TEST:
			assert(position==1);
			CheckIO(a, "if/while-test");
			src1 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_REG, REG_ACC);
			ins = new Instruction(OP_SET, dst, src1);
			AddIns(ins, insOffset);
			break;
		default:
			throw ExceptionVYPa(ERROR_INTERNAL, "Invalid operation with one parameter detected in expression analyzer.");
	}

	return 1;
}	

void ExprNodeOperation1::Print()
{
	std::cerr << "OPERATION: ";
	switch(m_NodeType)
	{
		case NODE_NOT:
			std::cerr << "NOT";
			break;
		case NODE_USELESS:
			std::cerr << "USELESS";
			break;
		case NODE_TEST:
			std::cerr << "TEST";
			break;
		default:
			std::cerr << "UNKNOWN";
			break;
	}
}


ExprNodeOperation2::ExprNodeOperation2(NodeType type)
	: ExprNode(type)
{}

void ExprNodeOperation2::CheckI(Type a, Type b, std::string op)
{
	if ((a.GetType() != TYPE_INT) || (b.GetType() != TYPE_INT))
	{
		std::stringstream ss;
		ss << "Operator '" << op << "' expects operants of type 'int', but operants are of types '" << a.GetText() << "' and '" << b.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

void ExprNodeOperation2::CheckIS(Type a, Type b, std::string op)
{
	if (!(((a.GetType() == TYPE_INT) && (b.GetType() == TYPE_INT)) || ((a.GetType() == TYPE_STRING) && (b.GetType() == TYPE_STRING))))
	{
		std::stringstream ss;
		ss << "Operator '" << op << "' expects operants of the same type 'int' or 'string', but operants are of types '" << a.GetText() << "' and '" << b.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

void ExprNodeOperation2::CheckIO(Type a, Type b, std::string op)
{
	if (a.GetType() == TYPE_STRING || b.GetType() == TYPE_STRING)
	{
		std::stringstream ss;
		ss << "Operator '" << op << "' expects operants of types 'int' or 'object', but operants are of types '" << a.GetText() << "' and '" << b.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

void ExprNodeOperation2::CheckISO(Type a, Type b, std::string op)
{
	if (a.GetType() != b.GetType())
	{
		std::stringstream ss;
		ss << "Operator '" << op << "' expects operants of the same type 'object', but operants are of types '" << a.GetText() << "' and '" << b.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

void ExprNodeOperation2::CheckAssignment(Type a, Type b)
{
	if (a.GetType() != b.GetType())
	{
		std::stringstream ss;
		ss << "Operator '=' expects operants of the same type 'int', 'string' or 'object', but operants are of types '" << a.GetText() << "' and '" << b.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
	if (a.GetType() == TYPE_OBJECT && !(PARSER->GetSymbolTable()->IsSubclass(b.GetSpecifier(), a.GetSpecifier())))
	{
		std::stringstream ss;
		ss << "Operator '=' expects object Rvalue to be a subclass of object LValue, but '" << b.GetText() << "' is not a subclass of '" << a.GetText() << "'";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

void ExprNodeOperation2::CheckCast(Type a, Type b)
{
	if (a.GetType() == b.GetType() && a.GetType() == TYPE_OBJECT)
	{
		// TODO check object compatibility
	}
	else if (a.GetType() != TYPE_STRING || b.GetType() != TYPE_INT)
	{
		std::stringstream ss;
		ss << "Operator 'cast' accepts only 'int' to 'string' or 'object' to 'object' conversions. Cast from '" << b.GetText() << "' to '" << a.GetText() << "' is not valid";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
}

void ExprNodeOperation2::CleanAfterStrings(unsigned SP, unsigned* insOffset, bool copyToStack)
{
	Value* dst;
	Value* src1;
	Instruction* ins;

	dst = new Value(VAL_STACK, SP-1);
	ins = new Instruction(OP_DESTROY, dst);
	AddIns(ins, insOffset);

	dst = new Value(VAL_STACK, SP-2);
	ins = new Instruction(OP_DESTROY, dst);
	AddIns(ins, insOffset);

	if (copyToStack)
	{
		dst = new Value(VAL_STACK, SP-2);
		src1 = new Value(VAL_REG, REG_TMP);
		ins = new Instruction(OP_SET, dst, src1);
		AddIns(ins, insOffset);
	}
}

unsigned ExprNodeOperation2::Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* src2;
	Value* dst;
	Instruction* ins;
	ExprNode* node;
	
	assert(position >= 2);
	Type a = stack->GetNode(position-2)->GetType();
	Type b = stack->GetNode(position-1)->GetType();

	switch (m_NodeType)
	{
		case NODE_SUB:
			CheckI(a, b, "-");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_SUBI, dst, src1, src2);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_MUL:
			CheckI(a, b, "*");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_MULI, dst, src1, src2);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_DIV:
			CheckI(a, b, "/");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_DIVI, dst, src1, src2);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_ADD:
			CheckIS(a, b, "+");
			m_DataType = a;

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			if (a.GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_REG, REG_TMP);
			}
			else
			{
				dst = new Value(VAL_STACK, stack->GetSP()-2);
			}
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_ADDI : OP_ADDS), dst, src1, src2);
			AddIns(ins, insOffset);
			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset);
			}
			stack->GetSP()--;

			break;
		case NODE_LES:
			CheckIS(a, b, "<");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			if (a.GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_REG, REG_TMP);
			}
			else
			{
				dst = new Value(VAL_STACK, stack->GetSP()-2);
			}
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_LTI : OP_LTS), dst, src1, src2);
			AddIns(ins, insOffset);
			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset);
			}
			stack->GetSP()--;

			break;
		case NODE_LEQ:
			CheckIS(a, b, "<=");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_REG, REG_TMP);
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_GTI : OP_GTS), dst, src1, src2);
			AddIns(ins, insOffset);

			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset, false);
			}

			src1 = new Value(VAL_REG, REG_TMP);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_NOT, dst, src1);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_GRT:
			CheckIS(a, b, ">");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			if (a.GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_REG, REG_TMP);
			}
			else
			{
				dst = new Value(VAL_STACK, stack->GetSP()-2);
			}
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_GTI : OP_GTS), dst, src1, src2);
			AddIns(ins, insOffset);
			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset);
			}
			stack->GetSP()--;

			break;
		case NODE_GEQ:
			CheckIS(a, b, ">=");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_REG, REG_TMP);
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_LTI : OP_LTS), dst, src1, src2);
			AddIns(ins, insOffset);

			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset, false);
			}

			src1 = new Value(VAL_REG, REG_TMP);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_NOT, dst, src1);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_EQ:
			CheckISO(a, b, "==");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			if (a.GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_REG, REG_TMP);
			}
			else
			{
				dst = new Value(VAL_STACK, stack->GetSP()-2);
			}
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_EQI : OP_EQS), dst, src1, src2);
			AddIns(ins, insOffset);

			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset);
			}

			stack->GetSP()--;

			break;
		case NODE_NEQ:
			CheckISO(a, b, "!=");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_REG, REG_TMP);
			ins = new Instruction(((m_DataType.GetType() == TYPE_INT) ? OP_EQI : OP_EQS), dst, src1, src2);
			AddIns(ins, insOffset);

			if (a.GetType() == TYPE_STRING)
			{
				CleanAfterStrings(stack->GetSP(), insOffset, false);
			}

			src1 = new Value(VAL_REG, REG_TMP);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_NOT, dst, src1, src2);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_AND:
			CheckIO(a, b, "&&");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_AND, dst, src1, src2);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_OR:
			CheckIO(a, b, "||");
			m_DataType = Type(TYPE_INT);

			src1 = new Value(VAL_STACK, stack->GetSP()-2);
			src2 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_STACK, stack->GetSP()-2);
			ins = new Instruction(OP_OR, dst, src1, src2);
			AddIns(ins, insOffset);
			stack->GetSP()--;

			break;
		case NODE_ASSIGNMENT:
			assert(position==2);
			node = stack->GetNode(position-2);
			if (node->GetNodeType() != NODE_VARIABLE && node->GetNodeType() != NODE_OBJ_VARIABLE)
			{
				throw ExceptionVYPa(ERROR_SEMANTIC, "Assignment Lvalue is not a variable");
			}
			CheckAssignment(a, b);

			if (node->GetNodeType() == NODE_VARIABLE)
			{
				src1 = new Value(VAL_STACK, stack->GetSP()-1);
				dst = new Value(VAL_BASE, ((ExprNodeVariable*)(node))->GetIndex());
				ins = new Instruction(((a.GetType() == TYPE_STRING) ? OP_COPY : OP_SET), dst, src1);
				AddIns(ins, insOffset);
			}
			else
			{
				if (node->GetType().GetType() == TYPE_STRING)
				{
					dst = new Value(VAL_REG, REG_TMP);
					src1 = new Value(VAL_STACK, stack->GetSP()-1);
					ins = new Instruction(OP_COPY, dst, src1);
					AddIns(ins, insOffset);
					dst = new Value(VAL_STACK, stack->GetSP()-2);
					src1 = new Value(VAL_IMID, ((ExprNodeObjectVariable*)(node))->GetIndex());
					src2 = new Value(VAL_REG, REG_TMP);
					ins = new Instruction(OP_SETWORD, dst, src1, src2);
					AddIns(ins, insOffset);
				}
				else
				{
					dst = new Value(VAL_STACK, stack->GetSP()-2);
					src1 = new Value(VAL_IMID, ((ExprNodeObjectVariable*)(node))->GetIndex());
					src2 = new Value(VAL_STACK, stack->GetSP()-1);
					ins = new Instruction(OP_SETWORD, dst, src1, src2);
					AddIns(ins, insOffset);
				}
			}

			if (a.GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_STACK, stack->GetSP()-1);
				ins = new Instruction(OP_DESTROY, dst);
				AddIns(ins, insOffset);
			}

			stack->GetSP()--;
			if (node->GetNodeType() != NODE_VARIABLE)
			{
				stack->GetSP()--;
			}

			break;
		case NODE_CAST:
			if (stack->GetNode(position-2)->GetNodeType() != NODE_TYPE)
			{
				throw ExceptionVYPa(ERROR_SEMANTIC, "Cast requires data type in parentheses, but there is not a type in there.");
			}
			CheckCast(a, b);
			m_DataType = a;
			src1 = new Value(VAL_STACK, stack->GetSP()-1);
			dst = new Value(VAL_REG, REG_CHUNK);
			ins = new Instruction(OP_INT2STRING, dst, src1);
			// TODO dynamic conversion of objects
			AddIns(ins, insOffset);
			AddIns(new Instruction(OP_SET, new Value(VAL_STACK, stack->GetSP()-1), new Value(VAL_REG, REG_CHUNK)), insOffset);
			break;
		default:
			throw ExceptionVYPa(ERROR_INTERNAL, "Invalid operation with two parameters detected in expression analyzer.");
	}

	return 2;
}

void ExprNodeOperation2::Print()
{
	std::cerr << "OPERATION: ";
	switch(m_NodeType)
	{
		case NODE_CAST:
			std::cerr << "CAST";
			break;
		case NODE_ASSIGNMENT:
			std::cerr << "ASSIGN";
			break;
		case NODE_MUL:
			std::cerr << "MUL";
			break;
		case NODE_DIV:
			std::cerr << "DIV";
			break;
		case NODE_ADD:
			std::cerr << "ADD";
			break;
		case NODE_SUB:
			std::cerr << "SUB";
			break;
		case NODE_LES:
			std::cerr << "LES";
			break;
		case NODE_LEQ:
			std::cerr << "LEQ";
			break;
		case NODE_GRT:
			std::cerr << "GRT";
			break;
		case NODE_GEQ:
			std::cerr << "GEQ";
			break;
		case NODE_EQ:
			std::cerr << "EQ";
			break;
		case NODE_NEQ:
			std::cerr << "NEQ";
			break;
		case NODE_AND:
			std::cerr << "AND";
			break;
		case NODE_OR:
			std::cerr << "OR";
			break;
		default:
			std::cerr << "UNKNOWN";
			break;
	}
}

ExprNodeType::ExprNodeType(Type type)
	: ExprNode(NODE_TYPE)
{
	m_DataType = type;
}

unsigned ExprNodeType::Evaluate(UNUSED ExprStack* stack, UNUSED unsigned position, UNUSED unsigned* insOffset)
{
	return 0;
}

void ExprNodeType::Print()
{
	std::cerr << "TYPE: " << m_DataType.GetText();
}


ExprNodeVariable::ExprNodeVariable(std::string name, unsigned index, Type type, bool assign)
	: ExprNode(NODE_VARIABLE),
	  m_Name(name),
	  m_Index(index),
	  m_Assign(assign)
{
	ASSERT_EMPTY(name);
	m_DataType = type;
}

unsigned ExprNodeVariable::Evaluate(ExprStack* stack, UNUSED unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* dst;
	Instruction* ins;

	if (!m_Assign)
	{
	       	src1 = new Value(VAL_BASE, m_Index);
		dst = new Value(VAL_STACK, stack->GetSP()++);
		if (m_DataType.GetType() == TYPE_STRING)
		{
			ins = new Instruction(OP_COPY, dst, src1);
		}
		else
		{
			ins = new Instruction(OP_SET, dst, src1);
		}
		AddIns(ins, insOffset);
	}

	return 0;
}

unsigned& ExprNodeVariable::GetIndex()
{
	return m_Index;
}

void ExprNodeVariable::Print()
{
	std::cerr << "VARIABLE: " << m_Name;
}

ExprNodeConst::ExprNodeConst(std::string value)
	: ExprNode(NODE_CONST),
	  m_ValueStr(value)
{
	m_DataType = Type(TYPE_STRING);
}

ExprNodeConst::ExprNodeConst(int value)
	: ExprNode(NODE_CONST),
	  m_ValueInt(value)
{
	m_DataType = Type(TYPE_INT);
}

unsigned ExprNodeConst::Evaluate(ExprStack* stack, UNUSED unsigned position, unsigned* insOffset)
{
	Value* src1;
	Value* dst;
	Instruction* ins;

	if (m_DataType.GetType() == TYPE_INT)
	{
		src1 = new Value(VAL_IMID, m_ValueInt);
	}
	else
	{
		src1 = new Value(VAL_IMID, m_ValueStr);
	}
	dst = new Value(VAL_STACK, stack->GetSP()++);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);

	return 0;
}

void ExprNodeConst::Print()
{
	std::cerr << "CONST " << ((m_DataType.GetType() == TYPE_INT) ? "INT" : "STR") << ": ";
	if (m_DataType.GetType() == TYPE_INT)
	{
		std::cerr << m_ValueInt;
	}
	else
	{
		std::cerr << m_ValueStr;
	}
}

ExprNodeFunction::ExprNodeFunction(std::string name, unsigned params, bool method)
	: ExprNode(NODE_FUNCTION),
	  m_Name(name),
	  m_Params(params),
	  m_Method(method)
{
	ASSERT_EMPTY(name);
}

void ExprNodeFunction::CheckTypes(Type a, Type b, unsigned index)
{
	if (a.GetType() != b.GetType())
	{
		std::stringstream ss;
		ss << "Function '" << m_Name << "' expects argument '" << index << "' of type '" << a.GetText() << "' but type '" << b.GetText() << "' was used";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}
	if (a.GetType() == TYPE_OBJECT && !(PARSER->GetSymbolTable()->IsSubclass(b.GetSpecifier(), a.GetSpecifier())))
	{
		std::stringstream ss;
		ss << "Function '" << m_Name << "' expects argument '" << index << "' of type '" << a.GetText() << "' but type '" << b.GetText() << "' was used and it is not a subclass of expected class";
		throw ExceptionVYPa(ERROR_TYPE, ss.str());
	}

}

unsigned ExprNodeFunction::Evaluate(ExprStack* stack, unsigned position, unsigned* insOffset)
{
	assert(position >= m_Params);

	Function* fun;
	if (!m_Method)
	{
	       	fun = PARSER->GetSymbolTable()->FindFunction(m_Name);
	}
	else
	{
		ExprNode* node = stack->GetNode(position-m_Params);
		Type type = node->GetType();
		if (type.GetType() != TYPE_OBJECT)
		{
			std::stringstream ss;
			ss << "Operator '.' requires parameter of type 'object', but parameter of type'" << type.GetText() << "' was found";
			throw ExceptionVYPa(ERROR_TYPE, ss.str());
		}
		Class* cls = PARSER->GetSymbolTable()->FindClass(type.GetSpecifier());
		fun = cls->FindFunction(m_Name);
		if (node->GetNodeType() == NODE_OBJ_KEYWORD && ((ExprNodeObjectKeyword*)node)->IsSuper())
		{
			if (fun->GetObject()->GetName() != cls->GetName())
			{
				std::stringstream ss;
				ss << "Method '" << m_Name << "' requested using keyword 'super' from class '" << cls->GetName() << "', but this method was inhereted and not overrited by that class";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
			}
			std::string origName = cls->GetName();
			cls = cls->GetParent();
			try
			{
				fun = cls->FindFunction(m_Name);
			}
			catch (ExceptionVYPa&)
			{
				std::stringstream ss;
				ss << "Method '" << m_Name << "' requested using keyword 'super' from class '" << origName << "' was not defined in any of the parent classes";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
			}
		}
	}

	fun->SetUsed();
	if (fun->GetParamCount() != PARAM_INF && m_Params != (unsigned)fun->GetParamCount())
	{
		std::stringstream ss;
		ss << "Function '" << m_Name << "'was called with '" << m_Params << "' parameters, but it has '" << fun->GetParamCount() << "'.";
		throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
	}

	Variable* var;
	ExprNode* node;
	Value* dst;
	Value* src1;
	Value* src2;
	Instruction* ins;

	for (unsigned i = 0; i < m_Params; i++)
	{
		node = stack->GetNode(position-i-1);
		if (fun->GetParamCount() != PARAM_INF)
		{
			var = fun->GetVariable(m_Params-i-1);
			CheckTypes(var->GetType(), node->GetType(), i);
		}
		else
		{
			std::stringstream ss;
			switch (node->GetType().GetType())
			{
				case TYPE_STRING:
					break;
				case TYPE_INT:
					src1 = new Value(VAL_STACK, stack->GetSP() - i - 1);
					dst = new Value(VAL_REG, REG_CHUNK);
					ins = new Instruction(OP_INT2STRING, dst, src1);

					AddIns(ins, insOffset);
					AddIns(new Instruction(OP_SET, new Value(VAL_STACK, stack->GetSP() - i - 1), new Value(VAL_REG, REG_CHUNK)), insOffset);
					break;
				default:
					ss << "Function 'print' expects parameters of type 'int' or 'string', but parameter of type '" << node->GetType().GetText() << "'was found";
					throw ExceptionVYPa(ERROR_TYPE, ss.str());
			}
		}
	}

	m_DataType = fun->GetType();

	unsigned vars;
	if (fun->GetParamCount() != PARAM_INF)
	{
	       	vars = fun->GetMaxVars();
	}
	else
	{
		vars = m_Params;
	}

    dst = new Value(VAL_STACK, stack->GetSP()+vars-m_Params);
	src1 = new Value(VAL_REG, REG_BP);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);
	dst = new Value(VAL_REG, REG_BP);
	src1 = new Value(VAL_REG, REG_SP);
	src2 = new Value(VAL_IMID, stack->GetSP() - m_Params);
	ins = new Instruction(OP_ADDI, dst, src1, src2);
	AddIns(ins, insOffset);

	dst = new Value(VAL_STACK, stack->GetSP()+vars-m_Params+1);
	src1 = new Value(VAL_REG, REG_SP);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);
	dst = new Value(VAL_REG, REG_SP);
	src1 = new Value(VAL_REG, REG_BP);
	src2 = new Value(VAL_IMID, vars + 3);
	ins = new Instruction(OP_ADDI, dst, src1, src2);
	AddIns(ins, insOffset);

	dst = new Value(VAL_STACK, -1);
	src1 = new Value(VAL_LABEL, fun->GetUniqueName());
	ins = new Instruction(OP_CALL, dst, src1);
	AddIns(ins, insOffset);

	if (fun->GetParamCount() != PARAM_INF)
	{
		for (unsigned i = 0; i < m_Params; i++)
		{
			node = stack->GetNode(position-i-1);
			if (node->GetType().GetType() == TYPE_STRING)
			{
				dst = new Value(VAL_STACK, stack->GetSP() + vars - m_Params - i - 1);
				ins = new Instruction(OP_DESTROY, dst);
				AddIns(ins, insOffset);
			}
		}
	}
	stack->GetSP() -= m_Params;

	dst = new Value(VAL_STACK, stack->GetSP());
	src1 = new Value(VAL_REG, REG_ACC);
	ins = new Instruction(OP_SET, dst, src1);
	AddIns(ins, insOffset);
	stack->GetSP()++;

	return m_Params;
}

void ExprNodeFunction::Print()
{
	std::cerr << "FUNCTION: " << m_Name << " (" << m_Params << ")";
}

ExprNode::ExprNode(NodeType type)
	: m_NodeType(type),
	  m_DataType(Type(TYPE_UNKNOWN))
{}

ExprNode::~ExprNode()
{}

Type& ExprNode::GetType()
{
	return m_DataType;
}

NodeType& ExprNode::GetNodeType()
{
	return m_NodeType;
}

ExprStack::ExprStack(unsigned insIndex)
	: m_InsIndex(insIndex),
	  m_StackPointer(0)
{}

ExprStack::~ExprStack()
{
	for (auto node : m_Nodes)
	{
		delete node;
	}
}

unsigned ExprStack::Evaluate(unsigned insOffset)
{
	int i = 0;
	unsigned ret;
	insOffset += m_InsIndex;
	unsigned orig = insOffset;
	while(m_Nodes.size() > 1)
	{
		ASSERT_NEGATIVE(i);
		ret = m_Nodes.at(i)->Evaluate(this, i, &insOffset);
		for (unsigned j = 0; j < ret; j++)
		{
			delete m_Nodes.at(i - j - 1);
			m_Nodes.erase(m_Nodes.begin() + i - j - 1);
		}
		i = i - ret + 1;
	}

	if (m_Nodes.size() != 1 || (m_Nodes.at(0)->GetNodeType() != NODE_RETURN && m_Nodes.at(0)->GetNodeType() != NODE_ASSIGNMENT && m_Nodes.at(0)->GetNodeType() != NODE_TEST && m_Nodes.at(0)->GetNodeType() != NODE_USELESS))
	{
		throw ExceptionVYPa(ERROR_INTERNAL, "Incomplete expression detected in expression stack. There is no final operation operation at the end of the expression");
	}

	return (insOffset - orig);
}

void ExprStack::AddNode(ExprNode* node)
{
	ASSERT_PTR(node);

	m_Nodes.push_back(node);
}

ExprNode* ExprStack::GetNode(unsigned index)
{
	return m_Nodes.at(index);
}

unsigned& ExprStack::GetSP()
{
	return m_StackPointer;
}

void ExprStack::Print()
{
	for (auto node : m_Nodes)
	{
		node->Print();
		std::cerr << std::endl;
	}
}

ExprAnalyzer::~ExprAnalyzer()
{
	for (auto stack : m_Stack)
	{
		delete stack;
	}
}

void ExprAnalyzer::Evaluate()
{
	unsigned instructions = 0;
	for (auto stack : m_Stack)
	{
		instructions += stack->Evaluate(instructions);
	}
}

void ExprAnalyzer::AddStack(ExprStack* stack)
{
	ASSERT_PTR(stack);

	m_Stack.push_back(stack);
}

void ExprAnalyzer::Print()
{
	std::cerr << "EXPRESSION STACKS:" << std::endl << std::endl;
	for (auto stack : m_Stack)
	{
		stack->Print();
		std::cerr << std::endl;
	}
}

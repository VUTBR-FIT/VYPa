#include "parser.h"
#include "symbol_table.h"
#include "instruction_tape.h"
#include <iostream>
#include <string>
#include <sstream>

std::string GetTabs(unsigned count)
{
	std::stringstream ss;
	for (unsigned i = 0; i < count; i++)
	{
		ss << "\t";
	}
	return ss.str();
}

Type::Type(BasicTypes type, std::string specifier)
	: m_Type(type),
	  m_Specifier(specifier)
{}

BasicTypes& Type::GetType()
{
	return m_Type;
}

std::string& Type::GetSpecifier()
{
	return m_Specifier;
}

std::string Type::GetText()
{
	std::stringstream ss;
	switch(m_Type)
	{
		case TYPE_VOID:
			ss << "void";
			break;
		case TYPE_INT:
			ss << "int";
			break;
		case TYPE_STRING:
			ss << "string";
			break;
		case TYPE_OBJECT:
			ss << "object (" << m_Specifier << ")";
			break;
		default:
			ss << "unknown";
			break;
	}

	return ss.str();
}

Variable::Variable(std::string name, Type type, unsigned block)
	: m_Name(name),
	  m_Type(type),
	  m_Block(block),
	  m_Used(false)
{
	ASSERT_EMPTY(name);
}

std::string& Variable::GetName()
{
	return m_Name;
}

Type& Variable::GetType()
{
	return m_Type;
}

unsigned& Variable::GetBlock()
{
	return m_Block;
}

void Variable::SetUsed()
{
	m_Used = true;
}

bool& Variable::GetUsed()
{
	return m_Used;
}

void Variable::Print(unsigned tabs)
{
	std::cerr << GetTabs(tabs) << m_Type.GetText() << " " << m_Name << std::endl;
}

Function::Function(std::string name, Type type, Class* object, bool buildIn)
	: m_Name(name),
	  m_Type(type),
	  m_Object(object),
	  m_ParamCount(0),
	  m_MaxVars(0),
	  m_Used(false),
	  m_BuildIn(buildIn)
{
	ASSERT_EMPTY(name);

	if (object != nullptr)
	{
		Variable* var = new Variable("this", Type(TYPE_OBJECT, ((Class *)object)->GetName()));
		AddParam(var);
	}

}

Function::~Function()
{
	for (auto var : m_Variables)
	{
		delete var;
	}
}

bool Function::IsBuildIn()
{
	return m_BuildIn;
}

void Function::AddParam(Variable* var)
{
	ASSERT_PTR(var);
	ASSERT_NEGATIVE(m_ParamCount);

	AddVariable(var);
	m_ParamCount++;
}

unsigned Function::AddVariable(Variable* var)
{
	ASSERT_NEGATIVE(m_ParamCount);
	ASSERT_PTR(var);

	for (auto it = m_Variables.begin(); it != m_Variables.end(); it++)
	{
		if ((*it)->GetName() == var->GetName() && (*it)->GetBlock() >= var->GetBlock())
		{
			std::stringstream ss;
		       	ss << "Second definition of variable '" << var->GetName() << "' in function '" << m_Name << "'.";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
	m_Variables.push_back(var);
	m_Names.push_back(var->GetName());

	return (m_Variables.size()-1);
}

void Function::SetInfiniteParams()
{
	ASSERT_NOT_EMPTY(m_Variables);

	m_ParamCount = PARAM_INF;
}

void Function::CleanVariables(unsigned block)
{
	if (m_MaxVars < m_Variables.size())
	{
		m_MaxVars = m_Variables.size();
	}

	for (unsigned i = m_ParamCount; i < m_Variables.size(); i++)
	{
		Variable* var = m_Variables.at(i);
		if (var->GetBlock() > block && var->GetType().GetType() == TYPE_STRING)
		{
			Value* dst = new Value(VAL_BASE, i);
			PARSER->GetInstructionTape()->AddInstruction(new Instruction(OP_DESTROY, dst));
		}
	}

	for (auto it = m_Variables.begin()+m_ParamCount; it != m_Variables.end();)
	{
		if ((*it)->GetBlock() > block)
		{
			if (!(*it)->GetUsed())
			{
				std::cerr << "WARNING: Variable '" << (*it)->GetName() << "' defined in function '" << m_Name << "' but not used" << std::endl;
			}
			delete (*it);
			it = m_Variables.erase(it);
		}
		else
		{
			it++;
		}
	}
}

std::string& Function::GetName()
{
	return m_Name;
}

Type& Function::GetType()
{
	return m_Type;
}

Variable* Function::FindVariable(std::string name, unsigned* index)
{
	ASSERT_EMPTY(name);

	for (int i = m_Variables.size()-1; i >= 0; i--)
	{
		if (m_Variables.at(i)->GetName() == name)
		{
			if (index != nullptr)
			{
				*index = i;
			}
			return m_Variables.at(i);
		}
	}
	std::stringstream ss;
	ss << "Variable '" << name << "' requested, but not found in function '" << m_Name << "'.";
	throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
}

Variable* Function::GetVariable(unsigned index)
{
	if (index >= m_Variables.size())
	{
		std::stringstream ss;
		ss << "Requesting variable at index '" << index << "' in function '" << m_Name << "', but function has only '" << m_Variables.size() << "' variables.";
		throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
	}
	return m_Variables.at(index);
}

int& Function::GetParamCount()
{
	return m_ParamCount;
}

unsigned& Function::GetMaxVars()
{
	return m_MaxVars;
}

Class* Function::GetObject()
{
	return m_Object;
}

void Function::SetUsed()
{
	m_Used = true;
}

bool& Function::GetUsed()
{
	return m_Used;
}

void Function::Evaluate()
{
	for (auto name : m_Names)
	{
		bool found = true;
		try
		{
			PARSER->GetSymbolTable()->FindFunction(name);
		}
		catch (ExceptionVYPa& ex)
		{
			found = false;
		}
		if (found)
		{
			std::stringstream ss;
			ss << "Variable '" << name << "' in function '" << m_Name << "' has the same name as a function defined in the code";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
		found = true;
		try
		{
			PARSER->GetSymbolTable()->FindClass(name);
		}
		catch (ExceptionVYPa& ex)
		{
			found = false;
		}
		if (found)
		{
			std::stringstream ss;
			ss << "Variable '" << name << "' in function '" << m_Name << "' has the same name as a class defined in the code";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
}

void Function::Print(unsigned tabs)
{
	std::cerr << GetTabs(tabs) << "FUNCTION " << m_Name << std::endl;
	std::cerr << GetTabs(tabs) << "return type: " << m_Type.GetText() << std::endl;
	std::cerr << GetTabs(tabs) << "ARGUMENTS (" ;
	if (m_ParamCount == PARAM_INF)
	{
		std::cerr << "infinity";
	}
	else
	{
		std::cerr << m_ParamCount;
	}
	std::cerr << "):" << std::endl;
	if (m_ParamCount != PARAM_INF)
	{
		for (int i = 0; i < m_ParamCount; i++)
		{
			m_Variables.at(i)->Print(tabs+1);
		}
	}
	std::cerr << GetTabs(tabs) << "SPACE FOR VARIABLES: " << m_MaxVars << std::endl << std::endl;
}

std::string Function::GetUniqueName()
{
	if (m_Object != nullptr)
	{
		return (this->m_Object->GetName() + "." + this->m_Name);
	}
	else
	{
		return this->m_Name;
	}
}

Class::Class(std::string name, std::string superior, bool buildIn)
	: m_Name(name),
	  m_Superior(superior),
	  m_Parent(nullptr),
	  m_BuildIn(buildIn)
{
	ASSERT_EMPTY(name);
}

Class::~Class()
{
	for (auto var : m_Variables)
	{
		delete var;
	}

	for (auto fun : m_Functions)
	{
		delete fun;
	}
}

bool Class::IsBuildIn()
{
	return m_BuildIn;
}

void Class::AddFunction(Function* fun)
{
	ASSERT_PTR(fun);

	for (auto it : m_Functions)
	{
		if (it->GetName() == fun->GetName())
		{
			std::stringstream ss; 
			ss << "Second definition of method '" << fun->GetName() << "' in class '" << m_Name << "'.";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
	m_Functions.push_back(fun);
}

void Class::AddVariable(Variable* var)
{
	ASSERT_PTR(var);

	for (auto it : m_Variables)
	{
		if (it->GetName() == var->GetName())
		{
			std::stringstream ss;
			ss << "Second definition of variable '" << var->GetName() << "' in class '" << m_Name << "'.";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
	m_Variables.push_back(var);
}

void Class::SetParent(Class* cls)
{
	m_Parent = cls;
}

std::string& Class::GetName()
{
	return m_Name;
}

std::string& Class::GetSuperior()
{
	return m_Superior;
}

Class* Class::GetParent()
{
	return m_Parent;
}

unsigned Class::GetVariablesCount()
{
	if (m_Parent == nullptr)
	{
		return m_Variables.size();
	}
	else
	{
		return (m_Parent->GetVariablesCount() + m_Variables.size());
	}
}

Function* Class::GetConstructor()
{
	return m_Constructor;
}

Variable* Class::FindVariable(std::string name, unsigned* index)
{
	ASSERT_EMPTY(name);

	for (unsigned i = 0; i < m_Variables.size(); i++)
	{
		if (m_Variables.at(i)->GetName() == name)
		{
			if (index != nullptr)
			{
				*index = i;
			}
			return m_Variables.at(i);
		}
	}

	if (m_Parent != nullptr)
	{
		return FindVariable(name, index);
	}

	std::stringstream ss;
	ss << "Variable '" << name << "' requested, but not found in class '" << m_Name << "'.";
	throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());

}

Variable* Class::GetVariable(unsigned index)
{
	std::vector<Class*> classes;
	Class* cls = this;
	while (cls != nullptr)
	{
		classes.push_back(cls);
		cls = cls->GetParent();
	}

	while (!classes.empty())
	{
		cls = classes.back();
		classes.pop_back();
		if (index < cls->GetVariablesCount())
		{
			return cls->m_Variables.at(index-m_Parent->GetVariablesCount());
		}
	}

	if (index >= m_Variables.size())
	{
		std::stringstream ss;
		ss << "Requesting variable at index '" << index << "' in class '" << m_Name << "', but class has only '" << m_Variables.size() << "' variables.";
		throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());

	}
	return m_Variables.at(index);
}

Function* Class::FindFunction(std::string name)
{
	ASSERT_EMPTY(name);

	for (auto fun : m_Functions)
	{
		if (fun->GetName() == name)
		{
			return fun;
		}
	}

	if (m_Parent != nullptr)
	{
		return m_Parent->FindFunction(name);
	}

	std::stringstream ss;
	ss << "Function '" << name << "' requested, but not found in class '" << m_Name << "'.";
	throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
}

void Class::Evaluate()
{
	for (auto it = m_Functions.begin(); it != m_Functions.end(); it++)
	{
		if ((*it)->GetName() == m_Name)
		{
			if ((*it)->GetType().GetType() != TYPE_VOID)
			{
				std::stringstream ss;
				ss << "Constructor of class '" << m_Name << "' expected to have return type 'void', but type '" << (*it)->GetType().GetType() << "' was found";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
			}
			if ((*it)->GetParamCount() != 0)
			{
				std::stringstream ss;
				ss << "Constructor of class '" << m_Name << "' expected to have '0' parameters, but '" << (*it)->GetParamCount() << "' parameters was found";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());			
			}
			m_Constructor = (*it);
			return;
		}
	}

	for (auto var : m_Variables)
	{
		for (auto fun : m_Functions)
		{
			if (var->GetName() == fun->GetName())
			{
				std::stringstream ss;
				ss << "Method '" << fun->GetName() << "' of class '" << m_Name << "'has the same name as a variable from the same class";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
			}
		}
	}

	for (auto var : m_Variables)
	{
		bool found = true;
		try
		{
			PARSER->GetSymbolTable()->FindFunction(var->GetName());
		}
		catch (ExceptionVYPa& ex)
		{
			found = false;
		}
		if (found)
		{
			std::stringstream ss;
			ss << "Variable '" << var->GetName() << "' of class '" << m_Name << "' has the same name as a function defined in the code";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
		found = true;
		try
		{
			PARSER->GetSymbolTable()->FindClass(var->GetName());
		}
		catch (ExceptionVYPa& ex)
		{
			found = false;
		}
		if (found)
		{
			std::stringstream ss;
			ss << "Variable '" << var->GetName() << "' of class '" << m_Name << "' has the same name as a class defined in the code";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}

	for (auto var : m_Functions)
	{
		bool found = true;
		try
		{
			PARSER->GetSymbolTable()->FindFunction(var->GetName());
		}
		catch (ExceptionVYPa& ex)
		{
			found = false;
		}
		if (found)
		{
			std::stringstream ss;
			ss << "Method '" << var->GetName() << "' of class '" << m_Name << "' has the same name as a function defined in the code";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
		found = true;
		try
		{
			PARSER->GetSymbolTable()->FindClass(var->GetName());
		}
		catch (ExceptionVYPa& ex)
		{
			found = false;
		}
		if (found)
		{
			std::stringstream ss;
			ss << "Method '" << var->GetName() << "' of class '" << m_Name << "' has the same name as a class defined in the code";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
}

void Class::CheckUsage()
{
	Function* constructor = GetConstructor();
	if (constructor != nullptr && !constructor->GetUsed() && !constructor->IsBuildIn())
	{
		std::cerr << "WARNING: Class '" << m_Name << "' was never used" << std::endl;
	}
	for (auto fun : m_Functions)
	{
		if (!fun->IsBuildIn() && !fun->GetUsed())
		{
			std::cerr << "WARNING: Method '" << fun->GetName() << "' of class '" << m_Name << "' was not used" << std::endl;
		}
	}
	for (auto var : m_Variables)
	{
		if (!var->GetUsed())
		{
			std::cerr << "WARNING: Variable '" << var->GetName() << "' of class '" << m_Name << "' was not used" << std::endl;
		}
	}
}

void Class::Print(unsigned tabs)
{
	std::cerr << GetTabs(tabs) << "CLASS  " << m_Name << std::endl;
	std::cerr << GetTabs(tabs) << "superior: " << m_Superior << std::endl;
	std::cerr << GetTabs(tabs) << "VARIABLES (" << m_Variables.size() << "):" << std::endl;
	for (auto var : m_Variables)
	{
		var->Print(tabs+1);
	}
	std::cerr << std::endl;
	std::cerr << GetTabs(tabs) << "METHODS (" << m_Functions.size() << "):" << std::endl;
	for (auto fun : m_Functions)
	{
		fun->Print(tabs+1);
	}
	std::cerr << std::endl;
	
}

SymbolTable::~SymbolTable()
{
	for (auto fun : m_Functions)
	{
		delete fun;
	}

	for (auto cls : m_Classes)
	{
		delete cls;
	}
}

Function* SymbolTable::GetMain()
{
	return m_Main;
}

void SymbolTable::AddFunction(Function* fun)
{
	ASSERT_PTR(fun);

	for (auto it : m_Functions)
	{
		if (it->GetName() == fun->GetName())
		{
			std::stringstream ss;
			ss << "Second definition of function '" << fun->GetName() << "' in global context.";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
	m_Functions.push_back(fun);
}

void SymbolTable::AddClass(Class* obj)
{
	ASSERT_PTR(obj);

	for (auto it : m_Classes)
	{
		if (it->GetName() == obj->GetName())
		{
			std::stringstream ss;
			ss << "Second definition of class '" << obj->GetName() << "' in global context.";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
	}
	m_Classes.push_back(obj);
}

void SymbolTable::Evaluate()
{
	for (auto fun : m_Functions)
	{
		if (fun->GetName() == "main")
		{
			if (fun->GetType().GetType() != TYPE_VOID)
			{
				std::stringstream ss;
				ss << "'main' function must have return type '" << Type(TYPE_VOID).GetText() << "', but return type '" << fun->GetType().GetText() << "' detected";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
			}
			if (fun->GetParamCount() != 0)
			{
				std::stringstream ss;
				ss << "'main' function must have '0' parameters, but '" << fun->GetParamCount() << "' parameters detected";
				throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());

			}
			m_Main = fun;
			fun->SetUsed();
			continue;
		}
		fun->Evaluate();
	}
	if (m_Main == nullptr)
	{
		throw ExceptionVYPa(ERROR_SEMANTIC, "Program requires definition of function 'main', but it was not found");
	}

	for (auto cls1 : m_Classes)
	{
		if (cls1->GetName() == "Object")
		{
			continue;
		}
		for (auto cls2 : m_Classes)
		{
			if (cls1->GetSuperior() == cls2->GetName())
			{
				cls1->SetParent(cls2);
				break;
			}
		}
		if (cls1->GetParent() == nullptr)
		{
			std::stringstream ss;
			ss << "Class '" << cls1->GetName() << "' should inherite from class '" << cls1->GetSuperior() << "', but no such class was found";
			throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
		}
		cls1->Evaluate();
	}
}

void SymbolTable::CheckUsage()
{
	for (auto fun : m_Functions)
	{
		if (!fun->IsBuildIn() && !fun->GetUsed())
		{
			std::cerr << "WARNING: Function '" << fun->GetName() << "' is not used" << std::endl;
		}
	}

	for (auto cls : m_Classes)
	{
		cls->CheckUsage();
	}
}

Function* SymbolTable::FindFunction(std::string name)
{
	ASSERT_EMPTY(name);

	for (auto fun : m_Functions)
	{
		if (fun->GetName() == name)
		{
			return fun;
		}
	}
	std::stringstream ss;
	ss << "Function '" << name << "' requested, but not found in global context.";
	throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
}

Class* SymbolTable::FindClass(std::string name)
{
	ASSERT_EMPTY(name);

	for (auto cls : m_Classes)
	{
		if (cls->GetName() == name)
		{
			return cls;
		}
	}
	std::stringstream ss;
	ss << "Class '" << name << "' requested, but not found in global context.";
	throw ExceptionVYPa(ERROR_SEMANTIC, ss.str());
}

bool SymbolTable::IsSubclass(std::string cls1, std::string cls2)
{
	Class* cls = FindClass(cls1);
	while (cls->GetParent() != nullptr)
	{
		if (cls->GetName() == cls2)
		{
			return true;
		}
		cls = cls->GetParent();
	}
	return false;
}

void SymbolTable::Print()
{
	std::cerr << "GLOBAL FUNCTIONS (" << m_Functions.size() << "): " << std::endl << std::endl;
	for (auto fun : m_Functions)
	{
		fun->Print(1);
	}

	std::cerr << "CLASSES (" << m_Classes.size() << "): " << std::endl << std::endl;
	for (auto cls : m_Classes)
	{
		cls->Print(1);
	}
}

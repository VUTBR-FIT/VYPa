%{
#include <iostream>
#include "comp.tab.h"
#define YY_DECL extern "C" int yylex()
int invalidToken = 0;
%}
%x comment
%x matched_escape
%option noyywrap
%%

[ \t\n\r]* 	;
\+		{ return TOK_PLUS; }
\-		{ return TOK_MINUS; }
\*		{ return TOK_TIMES; }
\/		{ return TOK_DIVIDE; }
\(		{ return TOK_BRACKET_L; }
\)		{ return TOK_BRACKET_R; }
\<\=		{ return TOK_LESS_EQUAL; }
\<		{ return TOK_LESS; }
\>\=		{ return TOK_GREAT_EQUAL; }
\>		{ return TOK_GREAT; }
\:		{ return TOK_COLON; }
\;		{ return TOK_SEMICOLON; }
\,		{ return TOK_COMMA; }
\.		{ return TOK_DOT; }
\{		{ return TOK_PARENT_BRACKET_L; }
\}		{ return TOK_PARENT_BRACKET_R; }
\&\&		{ return TOK_AND; }
\|\|		{ return TOK_OR; }
\=\=		{ return TOK_EQUAL; }
\!\=		{ return TOK_NOT_EQUAL; }
\!		{ return TOK_NOT; }
\=		{ return TOK_ASSIGN; }


0|[1-9][0-9]*			{ yylval.ival = atoi(yytext); return TOK_INT; }
void    return (TOK_KW_VOID);
new     return (TOK_KW_NEW);
class   return (TOK_KW_CLASS);
else    return (TOK_KW_ELSE);
if      return (TOK_KW_IF);
int     return (TOK_KW_INT);
return  return (TOK_KW_RETURN);
string  return (TOK_KW_STRING);
super   return (TOK_KW_SUPER);
this    return (TOK_KW_THIS);
while   return (TOK_KW_WHILE);
[_a-zA-Z][_a-zA-Z0-9]*		{ yylval.sval = strdup(yytext); return TOK_ID; }
\"([^\\\"]|\\[^\"]|\\\")*\"	{ yylval.sval = strdup(yytext); return TOK_STRING; }

\/\/[^\n]*\n			;
\/\*				{ BEGIN(comment); }
<comment>[^\*]*			;
<comment>\*[^\/]		;
<comment>\*\/			{ BEGIN(INITIAL); }

.				{ invalidToken = 1; return TOK_INVALID;}
%%


#ifndef H_PASER
#define H_PARSER

#include <fstream>

#include "symbol_table.h"
#include "instruction_tape.h"
#include "expr_analyzer.h"
#include "global.h"

class Parser
{
public:
	// Parser access
	static Parser* GetParser();
	static Parser* m_Singleton;

	// Modules access
	SymbolTable* GetSymbolTable();
	InstructionTape* GetInstructionTape();
	ExprAnalyzer* GetExprAnalyzer();

	// Misccellaneous
	void Cleanup();
	bool SetOutput(char* file);
	void EndOfProgram();

	// Types
	void SetType(BasicTypes type);
	void SetType(char* name);

	// Function definition
	void DefineFunction(char* name);
	void AddParam(char* name);
	void EndFunction();

	// Class definition
	void DefineClass(char* name, char* superior);
	void EndClass();

	// Variable definition
	void DefineVariable(char* name);

	// Return statement
	void Return(bool expr);

	// Assign statement
	void Assign();
	void LValueAssign(bool object, char* name);

	// Condition statement
	void If();
	void Else();
	void EndIf();

	// While statement
	void BeginWhile();
	void While();
	void EndWhile();

	// Function and method call
	void FunctionCall(bool method);
	void LValueMethod(bool super, char* name);
	void AddArg();
	void FunctionCallEnd(char* name, bool method);
	void Super();

	// Expression
	void Int(int value);
	void String(char* value);
	void Id(char* name);
	void ObjectVar(char* name);
	void ObjectCreation(char* name);
	void This();
	void CastingType();
	void Cast();
	void Not();
	void Times();
	void Divide();
	void Plus();
	void Minus();
	void Less();
	void Lessequal();
	void Great();
	void Greatequal();
	void Equal();
	void Notequal();
	void And();
	void Or();
	void ParLeft();
	void ParRight();

	// Expression misc
	void EndExpr();
	void StartExpr();
	void ExprUseless();

	// Block
	void BeginBlock();
	void EndBlock();

private:
	Parser();

	unsigned m_JumpCounter;
	std::vector<unsigned> m_JumpStack;
	Type m_LastType;
	std::ofstream m_Output;
	Function* m_Function;
	Class* m_Class;
	SymbolTable* m_SymbolTable;
	InstructionTape* m_InstructionTape;
	unsigned m_AssignIndex;
	ExprStack* m_Expr;
	ExprAnalyzer* m_ExprAnalyzer;
	std::vector<unsigned> m_ParamCounts;
	unsigned m_Block;
};

#endif

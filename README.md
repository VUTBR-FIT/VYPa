# VYPa - 2018/2019
[![pipeline status](https://gitlab.com/VUTBR-FIT/VYPa/badges/master/pipeline.svg)](https://gitlab.com/VUTBR-FIT/VYPa/commits/master)
## Authors

- Lukáš Černý (xcerny63)
- Kamil Michl (xmichlXY)

## Scripts
- build & run program
``` bash
make
make all
./comp INPUT_FILE
```
- build for tests and run tests
``` bash
make tests
make tests_run
```